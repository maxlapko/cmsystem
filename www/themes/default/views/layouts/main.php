<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?= $this->getPageTitle(); ?></title>
	<meta name="viewport" content="width=device-width">

	<style>
		body {
			padding-top: 60px;
			padding-bottom: 40px;
		}
	</style>
    <?php
        Yii::app()->getClientScript()
            ->registerCoreScript('yiiactiveform')
            ->registerScriptFile('/js/main.js', CClientScript::POS_END)
            ->registerMetaTag($this->metaKeywords, 'keywords')
            ->registerMetaTag($this->metaDescription, 'description')
    ?>
</head>
<body>
<?php $this->widget('ext.widgets.admin.AdminWidget') ?>    
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
	your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to
	improve your experience.</p>
<![endif]-->

<div>
    <?php $this->widget('application.modules.languages.widgets.LanguageSelectorWidget') ?>
</div>

<?php
$this->widget('zii.widgets.CMenu', array(
    'items' => array(
        array('label' => 'News', 'url' => array('/news/news/index')),
        array('label' => 'Pages', 'url' => array('/pages/pages/index')),
        array('label' => 'Blog', 'url' => array('/blog/posts/index')),
        
    ),
)); ?>
    <div class="container">
        <?= $content; ?>    
    </div>
</body>
</html>