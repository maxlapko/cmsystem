<?php

$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'id' => 'post-list',
    'itemView' => '_view',
));
