<div class="post-item">
    <p class="date"><?= $data->published_at; ?></p>
    <p class="title"><?= CHtml::encode($data->getI18n('title')); ?></p>
    <div>
        <?= $data->getI18n('short_content'); ?>
    </div>
    <p class="read_more"><?= CHtml::link('Read more &raquo;', $data->getUrl());?></p>
</div>