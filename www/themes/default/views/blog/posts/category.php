<?php
$this->breadcrumbs = array(
    'Blog' => array('index'),
    'Category ' . $category->getI18n('name')
);
?>
<h1>Posts from category: <?= $category->getI18n('name') ?></h1>
<?php $this->renderPartial('_list', compact('dataProvider')); ?>
