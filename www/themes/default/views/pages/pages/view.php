<?php
// Template Name: General
/** @var PagesController $this */
$this->breadcrumbs=array(
    'Pages' => array('/pages/pages/index'),
    $model->getI18n('title')
);
?>

<div class="pages-items">
    <p class="date"><?= $model->created_at; ?></p>
    <h2><?= h($model->getI18n('title')); ?></h2>
    <p class="desc">
        <?= $this->processText($model->getI18n('content')); ?>
    </p>
</div>