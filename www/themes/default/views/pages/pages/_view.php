<div class="page-item">
    <p class="date"><?= $data->created_at; ?></p>
    <p class="title"><?= $data->getI18n('title'); ?></p>
    <p class="read_more"><?= CHtml::link('Читать дальше &raquo;', $data->getUrl());?></p>
</div>