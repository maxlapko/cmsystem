<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form TbActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Login';
?>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'     => 'login-form',
    'type' => TbHtml::FORM_LAYOUT_HORIZONTAL,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array(
        'class' => 'well',
        'style' => 'width: 500px; margin: 0 auto;',
    ),
)); ?>
<fieldset>
    <legend>Login</legend>
    <?= $form->errorSummary($model) ?>
    <?= $form->textField($model, 'username', array('class' => 'input-medium', 'placeholder' => 'Email', 'prepend' => TbHtml::icon(TbHtml::ICON_USER))); ?>
    <?= $form->passwordField($model, 'password', array('class' => 'input-medium', 'placeholder' => 'Password', 'prepend' => TbHtml::icon(TbHtml::ICON_LOCK))); ?>
    <?= TbHtml::submitButton('Login', array('color' => TbHtml::BUTTON_COLOR_PRIMARY)); ?>
</fieldset>

<?php $this->endWidget(); ?>