<?php

class MenusController extends BackendController
{
    public $modelName = 'Menu';
    public $redirectTo = array('admin');
    public $defaultAction = 'admin';
    
    public function actionCreate()
    {
        $this->params['code'] = isset($_GET['code']) ? $_GET['code'] : '';
        unset($_GET['code']);
        parent::actionCreate();
    }
}
