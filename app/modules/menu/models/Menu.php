<?php

/**
 * This is the model class for table "menus".
 *
 * The followings are the available columns in table 'menus':
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $created_at
 */
class Menu extends EActiveRecord 
{

    public static function model($className = __CLASS__) 
    {
        return parent::model($className);
    }

    public function tableName() 
    {
        return 'menus';
    }
    
    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created_at',
                'updateAttribute'   => null
            )
        );
    }
    
    public function relations()
    {
        $alias = $this->getTableAlias(false, false);
        return array(
            'rootItem' => array(self::HAS_ONE, 'MenuItem', 'menu_id', 'condition' => "rootItem.level = 1")
        );
    }

    public function rules() 
    {
        return array(
            array('name, code', 'required'),
            array('code', 'unique'),
            array('name', 'length', 'max' => 255),
            array('created_at', 'safe'),
            array('id, name, code', 'safe', 'on' => 'search'),
        );
    }

    public function attributeLabels() 
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'created_at' => 'Created Date',
        );
    }

    public function search() 
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('Code', $this->code, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public static function getByCode($code)
    {
        return self::model()->findByAttributes(array('code' => $code));
    }
    
    public function getTree($lang = null)
    {
        static $tree = array();
        
        if (isset($tree[$this->id])) {
            return $tree[$this->id];
        }
        if ($this->rootItem === null) {
            return array();
        }
        return $tree[$this->id] = MenuItem::model()->withLang($lang)->findAll(array(
            'condition' => 'root = :root AND menu_id = :menuId', 
            'order' => 'lft',
            'params' => array(':root' => $this->rootItem->id, ':menuId' => $this->id)
        ));
    }
    
    public function listData($lang = null)
    {
        $tree = $this->getTree($lang);
        if (count($tree) === 0) {
            return array();
        }
        $menu = array($tree[0]->id => 'Without Parent');
        unset($tree[0]);
        foreach ($tree as $node) {
            $menu[$node->id] = str_repeat('&nbsp;&nbsp;', $node->level - 1) . $node->getI18n('name');
        }
        return $menu;
    }
    
    /**
     * Get array for menu widget
     * @return array
     */
    public function getMenu($lang = null)
    {
        $tree = $this->getTree($lang);
        if (count($tree) < 2) {
            return array();
        }
        unset($tree[0]);
        
        // Trees mapped
        $trees = array();
        $l = 0;
        // Node Stack. Used to help building the hierarchy
        $stack = array();

        foreach ($tree as $node) {
            $item = array(
                'url'   => HApp::createMenuUrl($node->url),
                'items' => array(),
                'label' => $node->getI18n('name'),
                'level' => $node->level,                
            );
            $item['active'] = HApp::detectMenuActive($item['url'], $node->active_condition);
            // Number of stack items
            $l = count($stack);

            // Check if we're dealing with different levels
            while ($l > 0 && $stack[$l - 1]['level'] >= $node->level) {
                array_pop($stack);
                $l--;
            }

            // Stack is empty (we are inspecting the root)
            if ($l == 0) {
                // Assigning the root node
                $i = count($trees);
                $trees[$i] = $item;
                $stack[] = & $trees[$i];
            } else {
                // Add node to parent
                $i = count($stack[$l - 1]['items']);
                $stack[$l - 1]['items'][$i] = $item;
                $stack[] = & $stack[$l - 1]['items'][$i];
            }
        }
        return $trees;
    }
    
    protected function beforeDelete()
    {
        MenuItem::model()->deleteAll('menu_id = ' . $this->id);
        return parent::beforeDelete();
    }


    protected function afterSave()
    {
        parent::afterSave();
        if ($this->getIsNewRecord()) {
            $item = new MenuItem;
            $item->url = '#';
            $item->menu_id = $this->id;
            $item->saveNode(false);
            $item->setI18n(
                array('parent_id' => $item->id, 'name' => 'root'), 
                Yii::app()->getModule('languages')->getDefault(), 
                true
            );
        }
    }
}