<?php

class MenuModule extends EWebModule
{
    public $defaultController = 'menus';
    
	public function init()
	{
        parent::init();
		// this method is called when the module is being created
		// you may place code here to customize the module or the application
        
		// import the module-level models and components
		$this->setImport(array(
			'menu.models.*',
		));
	}
    
    public function getMenu($code, $throwException = true) 
    {
        $menu = $this->getByCode($code, $throwException);
        if ($menu === null) {
            return array();
        }
        return $menu->getMenu();
    }
    
    public function getByCode($code, $throwException = true)
    {
        $menu = Menu::getByCode($code);
        if ($menu === null && $throwException) {
            throw new CException('The menu "' . $code . '" was not found.');                
        }
        return $menu;
    }
}
