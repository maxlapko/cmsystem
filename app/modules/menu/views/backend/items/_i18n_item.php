<?php $prefix = 'MenuItem[translateattrs][' . $lang['id'] . ']'; ?>
<div class="control-group ">
    <?= CHtml::activeLabelEx($model, 'name', array('class' => 'control-label', 'id' => "label-name-{$lang['id']}")); ?>
    <div class="controls">
        <?= CHtml::hiddenField($prefix . '[lang_id]', $lang['id'])?>
        <?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'value' => $model->name,
            'name' => $prefix . '[name]',
            'htmlOptions' => array('id' => "name-{$lang['id']}"),
            'sourceUrl' => array('/menu/items/suggest', 'lang' => $lang['id']),
            'options' => array(
                'minLength' => 3,
                'select' => 'js:function(event, ui) {
                    if (ui.item) {
                        $("#suggest-url").text(ui.item.url);
                    }
                }'
            )
        )) ?>
        <?= $form->error($model, 'name', array('inputID' => "name-{$lang['id']}")) ?>
    </div>
</div>