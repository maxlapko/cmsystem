<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $item->getFormId(),
        'action' => array('/menu/items/edit', 'menuId' => $item->menu_id, 'id' => $item->id),
        'type' => 'horizontal',
        'enableClientValidation' => false,
        'enableAjaxValidation' => false
    ));
?>
<fieldset>
    <?php $this->renderPartial('_i18ns', array('model' => $item, 'form' => $form)) ?>    
    <?= $form->dropDownListRow($item, 'parent_id', $parents, array('encode' => false, )); ?>
    <?= $form->textFieldRow($item, 'url'); ?>
    <div class="control-group ">
        <div class="controls">
            <a href="#" id="suggest-url"></a>
        </div>
    </div>
</fieldset>
<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $item->getIsNewRecord() ? 'Create' : 'Save')); ?>
</div>
<?php $this->endWidget(); ?>
<?php Yii::app()->getClientScript()->registerScript('suggest-url', '
    $("#suggest-url").click(function(e) {
        e.preventDefault();
        if ($(this).text() !== "") {
            $("#MenuItem_url").val($(this).text());
            $(this).text("");
        }
    });
    $("#' . $item->getFormId() . '").submit(function() {
        var form = $(this),
            submitButton = form.find(":submit");
        form.data("dataType", "json");
        if (submitButton.hasClass("disabled")) {
            return false;
        }
        submitButton.attr("disabled", "disabled").addClass("disabled");
        CMS.submitForm(form, function(err, rsp) {
            if (err) {
                console.log(err);
                submitButton.removeAttr("disabled").removeClass("disabled");
                return;
            } else if (rsp.target && rsp.html) {
                $(rsp.target).html($(rsp.target, $("<div>" + rsp.html + "</div>")).html());
            }
            if (rsp.success) {
                CMS.popup.el.modal("hide");
            }
        });
        return false;
    }); 

');

