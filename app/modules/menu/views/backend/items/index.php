<?= TbHtml::link(
    'Create new item', 
    array('/menu/items/edit', 'menuId' => $menu->id), 
    array('class' => 'btn btn-success', 'data-op' => 'modal', 'data-title' => 'Edit')
); ?>
<br />
<?php $this->renderPartial('_list', compact('menu', 'provider')) ?>