<?php
/* @var $this MenusController */
/* @var $model Menu */
/* @var $form EActiveForm */
?>
<br />
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'     => $model->getFormId(),
    'type' => TbHtml::FORM_LAYOUT_HORIZONTAL,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
)); ?>
<fieldset>
	<?= $form->errorSummary($model); ?>
    
    <?= $form->textFieldRow($model, 'code', array('maxlength' => 64)); ?>
    
    <?= $form->textFieldRow($model, 'name', array('maxlength' => 128)); ?>

</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton($model->getIsNewRecord() ? 'Create' : 'Save', array('color' => TbHtml::BUTTON_COLOR_PRIMARY)),
    TbHtml::submitButton($model->getIsNewRecord() ? 'Create and continue' : 'Save and continue', array('name' => 'continue', 'color' => TbHtml::BUTTON_COLOR_SUCCESS))
)); ?>
<?php $this->endWidget(); ?>