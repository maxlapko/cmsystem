<?php
/* @var $this MenusController */
/* @var $model Menu */

$this->breadcrumbs = array(
    'Menus' => array('admin'),
    'Update',
);
?>

<h1>Update Menu <?= h('"' . $model->name . '"'); ?></h1>

<?= $this->renderPartial('_form', array('model' => $model)); ?>
