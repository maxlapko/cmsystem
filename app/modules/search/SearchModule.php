<?php

class SearchModule extends CWebModule 
{
    public $searchModules = array();
    
    public $perPage = 30;

    public function search($q, $params = array()) 
    {
        $app = Yii::app();
        $result = array('models' => array(), 'count'  => 0);
        
        if (empty($this->searchModules) || mb_strlen($q, $app->charset) < 3) {
            return $result;
        }
        
        $searches = array(
            'count' => array(),
            'search' => array(),
        );
        foreach ($this->searchModules as $id) {
            $data = $app->getModule($id)->search();
            $searches['count'][] = strtr($data['sql'], array('{select}' => '1'));
            $searches['search'][] = strtr($data['sql'], array('{select}' => $data['select']));
        }
        $bind = $this->_getParams($q, $params);
        
        $result['count'] = $this->_getCount($searches['count'], $bind);
        
        $result['models'] = $this->_getData(
            $searches['search'], 
            array('bind' => $bind, 'count' => $result['count'], 'p' => $params)
        );
        
        return $result;
    }
    
    public function suggest($q, $lang = null)
    {
        $result = $this->search($q, array('lang' => $lang));
        if ($result['count'] == 0) {
            return array();
        }
        $names = array();
        foreach ($result['models'] as $page) {
            $names[] = array(
                'id' => $page->id, 
                'label' => get_class($page) . ': ' . $page->getI18n('title', false, $lang), 
                'value' => $page->getI18n('title', false, $lang), 
                'url' => $page->getUrl()
            );
        }
        return $names;
    }
    
    private function _getCount($sql, $params)
    {
        $sql = 'SELECT COUNT(*) FROM (' . implode(' UNION ',  $sql) . ') AS t';
        return Yii::app()->getDb()->createCommand($sql)->queryScalar($params);
    }
    
    private function _getData($sql, $params)
    {        
        $offset = isset($params['p']['page']) ? ((int) $params['p']['page'] - 1) * $this->perPage : 0;
        if ($offset >= $params['count']) {
            return array();
        }
        $sql = implode($sql, ' UNION ') . ' LIMIT ' . $offset . ', ' . $this->perPage;
        $tmp = Yii::app()->getDb()->createCommand($sql)->queryAll(true, $params['bind']);
        
        $result = array();
        foreach ($tmp as $e) {
            $result[$e['model_type']][] = $e['id'];
        }
        foreach ($result as $key => $ids) {
            $result[$key] = CActiveRecord::model($key)->withLang()->findAllByPk($ids, array('index' => 'id'));
        }
        $data = array();
        foreach ($tmp as $e) {
            if (isset($result[$e['model_type']][$e['id']])) {
                $data[] = $result[$e['model_type']][$e['id']];
            }
        }
        return $data;
    }
    
    private function _getParams($q, $params)
    {
        return array(
            ':lang' => isset($params['lang']) ? $params['lang'] : Yii::app()->getLanguage(), 
            ':q' => '%' . strtr($q, array('%'=>'\%', '_'=>'\_', '\\'=>'\\\\')) . '%'
        );
    }
}
