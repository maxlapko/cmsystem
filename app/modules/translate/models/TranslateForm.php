<?php

class TranslateForm extends CFormModel
{

    public $translations;
    
    protected $_file;
    protected $_fileContent;
    
    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // username and password are required
            array('translations', 'required'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'translations'   => 'Translations',
        );
    }
    
    public function setFile($file)
    {
        $this->_file = $file;
    }
    
    public function getTranslations()
    {
        if ($this->translations === null) {
            $this->translations = $this->getFileContent();
        }
        return $this->translations;
    }
    
    public function getFileContent()
    {
        if ($this->_fileContent === null) {
            $this->_fileContent = require $this->_file;
        }
        return $this->_fileContent;
    }
    
    public function perform()
    {
        $initial = $this->getFileContent();
        if (count($this->translations) !== count($initial)) {
            return false;
        }
        $i = 0;
        foreach ($initial as $key => &$value) {
            $value = $this->translations[$i++];
        }
        $array = var_export($initial, true);
        return file_put_contents($this->_file, "<?php\nreturn $array;");
    }

}
