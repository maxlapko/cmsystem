<?php
$this->pageTitle = 'Edit translations';
$this->breadcrumbs=array(
	'Manage translations' => array('admin'),
	'Edit translations',
);
$i = 0;
?>

<h3>Edit <?= "$lang - $cat" ?> translations</h3>


<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'translate-form',
        'enableClientValidation' => true,
        'type' => 'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
?>
<fieldset>
    <?php foreach ($model->getTranslations() as $key => $t) : ?>
    <?php $i++; ?>
    <div class="control-group">
        <label class="control-label required" for="<?= 't' . $i ?>"><?= $key ?></label>
        <div class="controls">
            <input class="span8" name="TranslateForm[translations][]" id="<?= 't' . $i ?>" type="text" value="<?= h($t) ?>" />
        </div>
            
    </div>
    <?php endforeach ?>
</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton('Save', array('color' => TbHtml::BUTTON_COLOR_PRIMARY))
))?>
<?php $this->endWidget(); ?>
