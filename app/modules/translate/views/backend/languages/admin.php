<?php
$this->pageTitle = 'Manage translations';
$this->breadcrumbs = array(
	'Manage translations'
);
?>
<?php foreach ($info as $key => $files) :?>
<div>
    <h3><?= h($langs[$key]['name']) ?></h3>
    <h4>Categories:</h4>
    <ul class="nav nav-tabs nav-stacked">
        <?php foreach ($files as $f) : ?>
        <li><?= CHtml::link(pathinfo($f, PATHINFO_FILENAME), array('/translate/languages/edit', 'lang' => $key, 'cat' => pathinfo($f, PATHINFO_FILENAME))) ?></li>
        <?php endforeach ?>
    </ul>
</div>
<?php endforeach ?>
