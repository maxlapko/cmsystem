<?php

class TranslateModule extends EWebModule 
{
    public $defaultController = 'languages';
    public $messagePath = 'application.messages';
    
    public function init()
	{
        parent::init();
        
		// import the module-level models and components
		$this->setImport(array(
			'translate.models.*',
		));
	}
    
    public function getMessagePath()
    {
        return Yii::getPathOfAlias($this->messagePath);
    }
}
