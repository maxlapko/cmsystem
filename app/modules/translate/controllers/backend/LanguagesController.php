<?php

class LanguagesController extends BackendController
{
    public $defaultAction = 'admin';
    
	public function actionAdmin()
    {
        $langs = Yii::app()->getModule('languages')->listing();
        $info = $this->_getFiles($langs);
        $this->render('admin', compact('langs', 'info'));
    }
    
    public function actionEdit($lang, $cat)
    {
        $file = $this->getModule()->getMessagePath() . "/$lang/$cat.php";
        if (!file_exists($file)) {
            throw new CHttpException(404, 'Could not find translations');
        }
        $model = new TranslateForm;
        $model->setFile($file);
        if (isset($_POST['TranslateForm'])) {
            $model->setAttributes($_POST['TranslateForm']);
            if ($model->perform()) {
                Yii::app()->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 'Successfully saved.');
                $this->refresh();
            }
        }
        $this->render('edit', compact('model', 'lang', 'cat'));
    }
    
    protected function _getFiles($langs)
    {
        $messagePath = $this->getModule()->getMessagePath();
        $data = array();
        foreach ($langs as $key => $l) {
            $path = $messagePath . '/' . $key;
            $data[$key] = array();
            if (is_dir($path)) {
                $data[$key] = CFileHelper::findFiles($path, array('fileTypes' => array('php')));
            }
        }
        return $data;
    }
}