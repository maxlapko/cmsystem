<?php

class LanguagesController extends BackendController
{
    public $modelName = 'Language';
    public $redirectTo = array('admin');
    public $defaultAction = 'admin';
    
    public function actionCreate()
    {
        /** @var CActiveRecord $model */
        $model = new $this->modelName;

        if (isset($_POST[$this->modelName])) {
            $model->setAttributes($_POST[$this->modelName]);
            if ($model->validate()) {
                if ($model->is_default) {
                    $model->updateAll(array('is_default' => 0), 'is_default = 1');
                }
                $model->save(false);
                Yii::app()->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 'Successfully created.');
                $this->_redirect($model);
            }
        }

        $this->render('create', array('model' => $model));
    }
    
    public function actionUpdate($id)
    {
        $model = $this->loadModel($this->modelName, $id);
        
        if (isset($_POST[$this->modelName])) {
            $model->setAttributes($_POST[$this->modelName]);
            if ($model->validate()) {
                if ($model->is_default) {
                    $model->updateAll(array('is_default' => 0), 'is_default = 1');
                }
                $model->save(false);
                Yii::app()->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 'Successfully updated.');
                $this->_redirect($model);
            }
        }
        $this->render('update', array('model' => $model));
    }
    
    public function actionMove($id, $dir)
    {
        $model = parent::actionMove($id, $dir);
        $this->renderJson(array(
            'html' => $this->renderPartial('_list', array('model' => new Language('search')), true),
            'success' => true,
            'target' => '#' . $model->getGridId(false)
        ));
        
    }
}