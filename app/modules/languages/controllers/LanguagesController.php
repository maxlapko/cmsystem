<?php

class LanguagesController extends FrontendController
{
    public $modelName = 'Language';
    
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array('change'),
                'users' => array('*'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    /**
     * Change current language
     * @param string $lang
     */
    public function actionChange($lang)
    {
        $request = Yii::app()->getRequest();
        $referrer = $request->getUrlReferrer();
        $module = Yii::app()->getModule('languages');
        if ($lang !== Yii::app()->getLanguage() &&
            array_key_exists($lang, $module->listing())
        ) {
            $default = $lang === $module->getDefault();
            $referrer = strtr($referrer, array(
                '/' . Yii::app()->getLanguage() => $default ? '' : ('/' . $lang),
                $request->getHostInfo() => ''
            ));
            if (stripos($referrer, '/' . $lang) === false && !$default) {
                $referrer = '/' . $lang . $referrer;
            }
            $module->setLanguage($lang);
        }
        $this->redirect($referrer);
    }
}