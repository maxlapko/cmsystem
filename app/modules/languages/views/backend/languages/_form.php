<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
    ));
?>
<fieldset>
	<?= $form->errorSummary($model); ?>
    
    <?= $form->textFieldRow($model, 'id', array('class' => 'span8')); ?>        
    <?= $form->textFieldRow($model, 'name', array('class' => 'span8')); ?>        
    <?= $form->textFieldRow($model, 'locale', array('class' => 'span8')); ?>
    <?= $form->toggleButtonRow($model, 'is_default'); ?>
    <?= $form->toggleButtonRow($model, 'is_active'); ?>
    
</fieldset>
<?= HApp::saveButtons($model) ?>


<?php $this->endWidget(); ?>
