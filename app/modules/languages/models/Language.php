<?php

class Language extends EActiveRecord
{   
    public $id;
    public $name;
    public $locale;
    public $order;
    public $is_default;
    public $is_active;
    public $created_at;
    public $updated_at;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'languages';
    }

    public function rules()
    {
        return array(
            array('id, name', 'required'),
            array('id', 'length', 'max' => 2),
            array('name, locale', 'length', 'max' => 32),
            array('id', 'checkLanguage', 'type' => 'iso'),
            array('locale', 'checkLanguage', 'type' => 'locale'),
            array('id, name', 'unique'),
            array('is_active, is_default', 'safe'),
            array('id, name,', 'safe', 'on' => 'search'),
        );
    }
    
    public function checkLanguage($attribute, $params)
    {
        if ($this->hasErrors($attribute)) {            
            return;            
        }
        if ($params['type'] === 'iso') {
            try {
                CLocale::getInstance($this->$attribute);
            } catch (Exception $exc) {
                $this->addError($attribute, t('language.backend', 'Invalid iso language.'));
            }
        } elseif ($params['type'] === 'locale') {
            $locale = setlocale(LC_CTYPE, '0');
            if (setlocale(LC_CTYPE, $this->$attribute) === false) {
                $this->addError($attribute, t('language.backend', 'Invalid locale.'));
            }
            setlocale(LC_CTYPE, $locale);
        } elseif ($params['type'] === 'janrain') {
        
        }
    }

    public function attributeLabels()
    {
        return array(
            'id'               => 'ISO',
            'name'             => 'Name',
            'is_default'       => 'Default',
            'locale'           => 'Locale',
            'is_active'        => 'Active',
            'order'            => 'Order',
            'created_at'       => 'Create date',
            'updated_at'       => 'Update date',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name);

        return new CActiveDataProvider($this->resetScope(), array(
            'criteria'   => $criteria,
            'sort'       => array(
                'defaultOrder' => '`order` ASC',
            ),
            'pagination' => array('pageSize' => 20),
        ));
    }

    public function behaviors()
    {
        return array(
            'AutoTimestampBehavior' => array(
                'class'           => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at',
                'setUpdateOnCreate' => true
            ),
        );
    }
    
    /**
     * Get default language id
     * @staticvar array $default
     * @return array
     */
    public static function getDefault()
    {
        static $default;
        if ($default === null) {
            foreach (self::listing() as $key => $lang) {
                if ($lang['is_default'] == 1) {
                    $default = $key;
                    break;
                }
            }
            $default = 'en';
        }
        return $default;
    }
    
    public static function listing()
    {
        static $list;
        if ($list === null) {
            $list = array();
            $langs = Yii::app()->getDb()->createCommand('
                SELECT * FROM ' . self::model()->tableName() . ' WHERE `is_active` = 1 ORDER BY `order` ASC'
            )->queryAll();
            foreach ($langs as $lang) {
                $list[$lang['id']] = $lang;
            }
        }
        return $list;
    }


    protected function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            $this->order = $this->getMaxOrder() + 1;
        }
        return parent::beforeSave();
    }
    
    public function getEditLink($params = array())
    {
        $params['id'] = $this->id;
        return (HApp::isFront() ? param('backend.link') : '') . Yii::app()->createUrl('/languages/languages/update', $params);
    }
}