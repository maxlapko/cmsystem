<?php 
$currentLang = Yii::app()->getLanguage();
?>
<div><b><?= $languages[$currentLang]['name']?></b></div>
<?php unset($languages[$currentLang]) ?>
<ul>
<?php foreach ($languages as $lang) { ?>
    <li><?= CHtml::link($lang['name'], array('/languages/languages/change', 'lang' => $lang['id']))?></li>
<?php } ?>
</ul>
    