<?php

class LanguageSelectorWidget extends CWidget
{
    public function run()
    {
        $module = Yii::app()->getModule('languages');
        $this->render('selector', array('languages' => $module->listing(), 'default' => $module->getDefault()));
    }

}