<?php

class Setting extends CFormModel
{
    private $_file;
    private $_params;
    
    public static $fields = array(
        'project_name'     => array(
            array('project_name', 'required'),
        ),
        'mailchimp.apikey' => array(),
        'mailchimp.list'   => array(),
        'application_form.email' => array(
            array('application_form.email', 'required'),
            array('application_form.email', 'email'),
        ),
        'logo' => array(),
    );

    public function __construct($scenario = '')
    {
        $this->_file = Yii::getPathOfAlias(Yii::app()->getModule('settings')->settingPath) . '.php';
        $this->_params = require $this->_file;
        foreach (self::$fields as $key => $value) {
            if (!isset($this->_params[$key])) {
                $this->_params[$key] = '';
            }
        }
        parent::__construct($scenario);
    }

    public function getAttributes($names = null)
    {
        return $this->_params;
    }

    public function __get($name)
    {
        if (isset($this->_params[$name])) {
            return $this->_params[$name];
        }

        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        if (isset($this->_params[$name])) {
            $this->_params[$name] = $value;
        } else {
            parent::__set($name, $value);
        }
    }

    public function rules()
    {
        $keys = array_keys($this->_params);
        unset($keys[array_search('logo', $keys)]);
        $rules = array(
            array(implode(',', $keys), 'safe'),
            array(
                'logo', 'ext.components.image_processor.MImageValidator', 'allowEmpty' => trim($this->_params['logo']) !== '',
                'types' => array('jpg', 'png', 'jpeg'), 'maxSize' => 5 * 1024 * 1024, 
                'minWidth' => 20, 'minHeight' => 20
            ),
        );
        foreach ($keys as $key) {
            if (!empty(self::$fields[$key])) {
                $rules = array_merge($rules, self::$fields[$key]);
            }
        }
        return $rules;
    }
    
    public function behaviors()
    {
        return array(
            'mImage' => array(
                'class'          => 'ext.components.image_processor.MImageBehavior',
                'imageProcessor' => 'image' // image processor component name 
            )
        );
    }

    public function attributeLabels()
    {
        return array(
            
        );
    }

    public function perform()
    {
        if ($this->validate()) {
            if (($logo = CUploadedFile::getInstance($this, 'logo')) !== null) {
                $webroot = Yii::getPathOfAlias('webroot');
                if ($this->_params['logo'] && file_exists($webroot . $this->_params['logo'])) {
                    @unlink($webroot . $this->_params['logo']);
                }
                $this->uploadImage($logo, 'logo');
                $this->_params['logo'] = $this->getImageUrl('logo', 'orig');
            }
            return file_put_contents(
                $this->_file, '<?php return ' . var_export($this->_params, true) . ';'
            );
        }
        return false;
    }

}