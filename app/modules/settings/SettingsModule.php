<?php

class SettingsModule extends EWebModule 
{
    public $defaultController = 'settings';
    public $settingPath = 'application.config.params';
    
	public function init()
	{
        parent::init();
        
		$this->setImport(array(
			'settings.models.*',
		));
	}
    
}
