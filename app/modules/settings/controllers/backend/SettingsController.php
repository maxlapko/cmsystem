<?php

class SettingsController extends BackendController
{
	public $modelName = 'Member';
	public $redirectTo = array('admin');
    public $defaultAction = 'admin';
    
    public function actionAdmin()
    {
        $model = new Setting;
        if (isset($_POST['Setting'])) {
            $model->setAttributes($_POST['Setting']); 
            if ($model->perform()) {
                $this->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 'Successfully created.');
                $this->refresh();
            }
        }
        $this->render('admin', compact('model'));
    }
    
    public function actionFlushCache()
    {
        Yii::app()->getCache()->flush();
        $this->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 'Cache successfully flushed.');
        $this->redirect(array('admin'));
    }
}