<?php
$this->pageTitle = 'Manage settings';
$this->breadcrumbs = array(
	'Manage settings'
);
?>

<br />
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#settings">Manage settings</a></li>
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            System Actions
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><?= CHtml::link('Clear cache', array('/settings/settings/flushCache')) ?></li>
        </ul>
    </li>
</ul>

<div class="tab-content">
  <div class="tab-pane active" id="settings"><?php $this->renderPartial('_form', array('model' => $model)); ?></div>
</div>
