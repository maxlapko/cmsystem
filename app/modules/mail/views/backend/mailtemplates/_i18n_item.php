<?php $prefix = 'MailTemplate[translateattrs][' . $lang['id'] . ']'; ?>
<?= CHtml::hiddenField($prefix . '[lang_id]', $lang['id'])?>

<?= $form->textFieldRow($model, 'subject', array('class' => 'span8', 'id' => "subject-{$lang['id']}", 'name' => $prefix . '[subject]')); ?>        
<?= $form->redactorRow($model, 'content', array( 
    'htmlOptions' => array(
        'id' => "content-{$lang['id']}", 
        'name' => $prefix . '[content]',
    ),
    'editorOptions' => array('minHeight' => 100),
)); ?>
<?= $form->textAreaRow($model, 'content_plain', array('class' => 'span8', 'rows' => 5, 'id' => "content_plain-{$lang['id']}", 'name' => $prefix . '[content_plain]')); ?>