<?php
$this->pageTitle = 'Update mail template';
$this->breadcrumbs=array(
	'Manage mail templates' => array('admin'),
	'Update mail template',
);
?>

<h3>Update <?= '"' . CHtml::encode($model->getI18n('subject')) . '"' ?> mail template</h3>
<?php
$this->renderPartial('_form', array('model' => $model));
?>