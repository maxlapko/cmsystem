<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
    ));
?>
<fieldset>    
    <?php $this->renderPartial('_i18ns', array('model' => $model, 'form' => $form)) ?>    
    <?= $form->textFieldRow($model, 'from', array('class' => 'span8')); ?>        
    <?= $form->textFieldRow($model, 'from_name', array('class' => 'span8')); ?>        
    <?= $form->textFieldRow($model, 'bcc', array('class' => 'span8')); ?>
    
</fieldset>
<?= HApp::saveButtons($model) ?>


<?php $this->endWidget(); ?>
