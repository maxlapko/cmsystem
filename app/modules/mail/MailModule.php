<?php

class MailModule extends EWebModule 
{
    public $defaultController = 'mailtemplates';
    public $mailComponent = 'mail';
    
    public $defaultEmailParams = array(
        'from'     => 'test@test.com',
        'fromName' => 'Site support',
    );
    
	public function init()
	{
        parent::init();
        
		$this->setImport(array(
			'mail.models.*',
			'mail.components.*',
		));
	}
    
    /**
     * @return \MailMessage
     */
    public function createMessage()
    {
        return new MailMessage;
    }
    
    public function send(MailMessage $mail)
    {
        return Yii::app()->getComponent($this->mailComponent)->send($mail);
    }
    
}
