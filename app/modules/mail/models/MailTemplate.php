<?php

/**
 * This is the model class for table "mail_templates".
 *
 * The followings are the available columns in table 'mail_templates':
 * @property integer $id
 * @property string $token
 * @property string $from
 * @property string $from_name
 * @property string $bcc
 * @property string $created_at
 * @property string $updated_at
 */
class MailTemplate extends I18nActiveRecord
{
    public $from;
    public $from_name;
    public $bcc;
    public $created_at;
    public $updated_at;
    public $searchSubject;
    public $token;
    
    public $i18nModel = 'MailTemplateI18n';
    
    protected static $_templates = array();
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MailTemplate the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'mail_templates';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('from, from_name', 'required'),
            array('from, bcc', 'email'),
            array('from, bcc', 'length', 'max' => 255),
            array('from_name', 'length', 'max' => 128),
            array('translateattrs', 'safe'),
            array('searchSubject, token', 'safe'),
        );
    }    

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'i18ns' => array(self::HAS_MANY, 'MailTemplateI18n', 'parent_id', 'index' => 'lang_id')
        );
    }
    
    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created_at',
                'updateAttribute'   => 'updated_at',
                'setUpdateOnCreate' => true
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'token'     => 'Token',
            'from'      => 'From',
            'from_name' => 'From Name',
            'bcc'       => 'Bcc',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('token', $this->token, true);
        $criteria->compare('searchSubject', $this->searchSubject, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public function setTranslateAttrs($i18ns)
    {
        $this->setNestedRelation('i18ns', $i18ns);
        $this->clearDirtyAttributes($this->i18ns);
    }
    
    /**
     * Find template by token
     * @param string $token
     * @param string $lang
     * @param array $data
     * @return MailTemplate 
     */
    public static function findByToken($token, $data = array(), $lang = null)
    {
        $lang = $lang === null ? Yii::app()->getLanguage() : $lang;
        $cacheToken = $token . '.' . $lang; 
        if (!isset(self::$_templates[$cacheToken])) {
            $template = self::model()->resetScope()->withLang($lang)->findByAttributes(array('token' => $token));
            if ($template === null) {
                $params = Yii::app()->getModule('mail')->defaultEmailParams;
                $template = new MailTemplate;
                $template->token = $token;
                $template->from = $params['from'];
                $template->from_name = $params['fromName'];                
                $template->save(false);
                if (!empty($data)) {
                    $i18n = new MailTemplateI18n;
                    $i18n->parent_id = $template->id;
                    $defaultLanguage = Yii::app()->getModule('languages')->getDefault();
                    $i18n->lang_id = $defaultLanguage;
                    $i18n->content = implode(' ', array_keys($data));
                    $i18n->subject = 'undefined';
                    $i18n->save(false);
                    $template->i18ns = array($defaultLanguage => $i18n);
                }
            }
            self::$_templates[$cacheToken] = $template;
        }
        return self::$_templates[$cacheToken];
    }
}