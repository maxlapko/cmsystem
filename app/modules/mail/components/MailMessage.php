<?php

/**
 * Description of MailMessage
 *
 * @author mlapko
 */
class MailMessage extends YiiMailMessage
{
    /**
     * Set additional params for mail
     * @param string $token
     * @param string $lang
     * @param array $data
     * @return mixed 
     */
    public function setBody($token, $data = array(), $lang = null) 
    {
        $template = MailTemplate::findByToken($token, $data, $lang);
        if ($template->bcc) {
            $this->message->setBcc($template->bcc);            
        }
        $this->message->setSubject(strtr($template->getI18n('subject', false, $lang), $data));
        $this->message->setFrom($template->from, $template->from_name);
        
        return $this->message->setBody(strtr($template->getI18n('content', false, $lang), $data), 'text/html');
    }
}