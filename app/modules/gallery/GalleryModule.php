<?php

class GalleryModule extends EWebModule 
{
    public $defaultController = 'galleries';
    
	public function init()
	{
        parent::init();
        
		$this->setImport(array(
			'gallery.models.*',
		));
        Yii::app()->getModule('images');
	}
    
    public function getByCode($code, $throwException = true)
    {
        $gallery = Gallery::getByCode($code);
        if ($gallery === null && $throwException) {
            throw new CException('The gallery "' . $code . '" was not found.');
        }
        return $gallery;
    }
    
}
