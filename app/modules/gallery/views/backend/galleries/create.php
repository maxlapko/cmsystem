<?php
/* @var $this GalleriesController */
/* @var $model Gallery */

$this->breadcrumbs=array(
    'Galleries'=>array('admin'),
    'Create',
);
?>

<h1>Create Gallery</h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>