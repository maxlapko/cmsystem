<?php
/* @var $this GalleriesController */
/* @var $model Gallery */

$this->breadcrumbs = array(
    'Galleries' => array('admin'),
    'Update',
);
?>

<h1>Update gallery <?= CHtml::encode('"' . $model->name . '"'); ?></h1>
<div class="pull-right">
    <?php $this->widget('application.modules.images.widgets.Media.MediaLibraryWidget', array('model' => $model))?>
</div>

<?= $this->renderPartial('_form', array('model' => $model)); ?>
