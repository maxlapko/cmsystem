<?php

class AdminsModule extends EWebModule 
{
    public $defaultController = 'admins';
    
	public function init()
	{
        parent::init();
        
		$this->setImport(array(
			'admins.models.*',
		));
	}
    
}
