<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
    ));
?>
<fieldset>
    <?= $form->textFieldRow($model, 'username', array('class' => 'span8')); ?>        
    <?= $form->textFieldRow($model, 'email', array('class' => 'span8')); ?>
    <?= $form->textFieldRow($model, 'password', array('class' => 'span8')); ?> 
    <?= $form->dropDownListRow($model, 'status', $model::statuses()); ?>
    <?= $form->dropDownListRow($model, 'role', $model::roles()); ?>
    
</fieldset>
<?= HApp::saveButtons($model) ?>


<?php $this->endWidget(); ?>
