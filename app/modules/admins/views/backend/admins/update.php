<?php
$this->pageTitle = 'Update admin';
$this->breadcrumbs=array(
	'Manage admins' => array('admin'),
	'Update admin',
);
?>

<h3>Update <?= '"' . CHtml::encode($model->username) . '"' ?> admin</h3>
<?php
$this->renderPartial('_form', array('model' => $model));
?>