<?php

class m140319_110057_add_role_to_admins extends CDbMigration
{

    public function up()
    {
        $sql = <<< EOD
ALTER TABLE `admins`
ADD COLUMN `role`  varchar(32) NOT NULL DEFAULT 'admin' AFTER `status`;
EOD;
        $this->execute($sql);
    }

    public function down()
    {
        $this->dropColumn('admins', 'role');
    }
}