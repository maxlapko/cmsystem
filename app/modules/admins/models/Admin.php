<?php

class Admin extends BaseUser
{
    const STATUS_BLOCKED = '0';
    const STATUS_ACTIVE = '1';
    
    const ROLE_ADMIN  = 'admin';
    const ROLE_EDITOR = 'editor';
    
    public $email;
    public $password;
    public $status;
    public $role;
    public $created_at;
    public $updated_at;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'admins';
    }

    public function rules()
    {
        return array(
            array('username, email, role', 'required'),
            array('email', 'email'),
            array('email', 'unique'),
            array('password', 'required', 'on' => array('insert')),
            array('password', 'safe', 'on' => array('update')),
            array('username, email', 'length', 'max' => 255),
            array('status', 'safe'),
            array('username, email, status', 'safe', 'on' => 'search'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'         => 'ID',
            'username'    => 'Username',
            'email'      => 'Email',
            'role'       => 'Role',
            'password'   => 'Password',
            'created_at' => 'Create date',
            'updated_at' => 'Update date',
            'status'     => 'Status'
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('username', $this->username, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('role', $this->role);

        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'sort'       => array(
                'defaultOrder' => 'username ASC',
            ),
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));
    }

    public function behaviors()
    {
        return array(
            'AutoTimestampBehavior' => array(
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created_at',
                'updateAttribute'   => 'updated_at',
                'setUpdateOnCreate' => true
            ),
        );
    }
    
    public static function statuses($status = null)
    {
        static $statuses = array(
            self::STATUS_BLOCKED => 'Blocked',
            self::STATUS_ACTIVE => 'Active'
        );
        return $status === null ? $statuses : $statuses[$status];
    }
    
    public static function roles($role = null)
    {
        static $roles = array(
            self::ROLE_ADMIN => 'Admin',
            self::ROLE_EDITOR => 'Editor'
        );
        return $role === null ? $roles : $roles[$role];
    }
    
    protected function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            $this->password = $this->hashPassword($this->password);
        }
        return parent::beforeSave();
    }

}