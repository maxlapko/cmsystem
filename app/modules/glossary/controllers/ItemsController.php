<?php

class ItemsController extends FrontendController
{
    public $modelName = 'GlossaryItem';
    
    public function actionIndex($letter = null)
    {
        $letters = Yii::app()->getModule('glossary')->getLetters();
        
        $finder = GlossaryItem::model()->withLang();
        if (count($letters) > 0 && ($letter === null || !in_array($letter, $letters))) {
            $letter = $letters[0];
        }
        
        $finder->byLetter($letter ? $letter : 'qq');
        
        
        $dataProvider = new CActiveDataProvider($finder, array(
            'pagination' => array(
                'pageSize' => 15
            )
        ));
        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'letter' => $letter,
            'letters' => $letters
        ));
    }
}