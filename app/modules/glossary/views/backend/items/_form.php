<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
?>
<fieldset>
    <?php $this->renderPartial('_i18ns', array('model' => $model, 'form' => $form)) ?>
    
</fieldset>
<?= HApp::saveButtons($model) ?>


<?php $this->endWidget(); ?>
