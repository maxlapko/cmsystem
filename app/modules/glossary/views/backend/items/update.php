<?php
$this->pageTitle = 'Update glossary item';
$this->breadcrumbs=array(
	'Manage glossary' => array('admin'),
	'Update glossary item',
);
?>

<h3>Update <?= '"' . CHtml::encode($model->getI18n('title')) . '"' ?> glossary item</h3>
<?php
$this->renderPartial('_form', array('model' => $model));
?>