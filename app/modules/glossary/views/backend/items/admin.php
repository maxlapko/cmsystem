<?php
$this->pageTitle= 'Manage glossary';

$this->breadcrumbs=array(
	'Glossary',
);
?>

<h1>Manage glossary</h1>

<?= TbHtml::link('Create new glossary item', array('create'), array('class' => 'btn')); ?>
<br />

<?php
$this->renderPartial('_list', compact('model')); 
?>