<?php $prefix = 'GlossaryItem[translateattrs][' . $lang['id'] . ']'; ?>
<?= CHtml::hiddenField($prefix . '[lang_id]', $lang['id'])?>
<?= $form->textFieldRow($model, 'letter', array('class' => 'span8', 'id' => "letter-{$lang['id']}", 'name' => $prefix . '[letter]')); ?>        
<?= $form->textFieldRow($model, 'title', array('class' => 'span8', 'id' => "title-{$lang['id']}", 'name' => $prefix . '[title]')); ?>        
<?= $form->textAreaRow($model, 'description', array('class' => 'span8', 'rows' => 5, 'id' => "description-{$lang['id']}", 'name' => $prefix . '[description]')); ?>
