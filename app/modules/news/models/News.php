<?php

class News extends EActiveRecord
{
    const DRAFT = '0';
    const PUBLISHED = '1';
    const ARCHIVED = '2';
    
    public $title;
    public $content;
    public $created_at;
    public $updated_at;
    public $published_at;
    public $slug;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'news';
    }

    public function rules()
    {
        return array(
            array('title, content', 'required'),
            array('title', 'length', 'max' => 255),
            array('short_content, status, meta_title, meta_keywords, meta_description, published_at', 'safe'),
            array('title, status', 'safe', 'on' => 'search'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'               => 'ID',
            'title'            => 'Title',
            'content'          => 'Content',
            'slug'             => 'Slug',
            'short_content'    => 'Short text',
            'created_at'       => 'Create date',
            'updated_at'       => 'Update date',
            'published_at'     => 'Publish date',
            'status'           => 'Status',
            'meta_title'       => 'Title (SEO)',
            'meta_keywords'    => 'Keywords (SEO)',
            'meta_description' => 'Description (SEO)',
        );
    }

    public function getUrl()
    {
        return Yii::app()->createUrl('/news/news/view', array(
            'slug' => $this->slug,
        ));
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('status', $this->status);
        $criteria->compare('title', $this->title, true);

        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'sort'       => array(
                'defaultOrder' => 'published_at DESC',
            ),
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));
    }

    public function behaviors()
    {
        return array(
            'AutoTimestampBehavior' => array(
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created_at',
                'updateAttribute'   => 'updated_at',
                'setUpdateOnCreate' => true
            ),
            'slugable'          => array(
                'class'     => 'ext.behaviors.SlugBehavior',
                'scenarios' => array('insert'),
                'sourceAttribute' => function($owner) {
                    return trim($owner->slug) === '' ? $owner->title : $owner->slug;
                }
            ),
        );
    }
    
    protected function beforeSave()
    {
        $this->published_at = date('Y-m-d', strtotime($this->published_at));
        return parent::beforeSave();
    }

    protected function afterFind()
    {
        $this->published_at = date('Y-m-d', strtotime($this->published_at));
        return parent::afterFind();
    }

    public function getAllWithPagination($inCriteria = null)
    {
        if ($inCriteria === null) {
            $criteria = new CDbCriteria;
            $criteria->addCondition('active = 1');
            $criteria->order = 'created_at DESC';
        } else {
            $criteria = $inCriteria;
        }

        $pages = new CPagination($this->count($criteria));
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $items = $this->findAll($criteria);

        return array(
            'items' => $items,
            'pages' => $pages,
        );
    }
    
    public static function statuses($status = null)
    {
        static $statuses = array(
            self::DRAFT => 'Draft',
            self::PUBLISHED => 'Published',
            self::ARCHIVED => 'Archived',
        );
        return $status === null ? $statuses : $statuses[$status];
    }

}