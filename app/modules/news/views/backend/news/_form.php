<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
    ));
    $model->published_at = $model->published_at ? $model->published_at : date('Y-m-d');
?>
<fieldset>
	<?= $form->errorSummary($model); ?>
    
    <?= $form->textFieldRow($model, 'title', array('class' => 'span8')); ?>        
    <?= $form->textFieldRow($model, 'slug', array('class' => 'span8')); ?>        
    <?= $form->redactorRow($model, 'content', array(
        'editorOptions' => array('minHeight' => 100, 'air' => false)
    )); ?>
    <?= $form->redactorRow($model, 'short_content', array( 
        'editorOptions' => array('minHeight' => 50)
    )); ?>
    <?= $form->datepickerRow($model, 'published_at', array('options' => array('format' => 'yyyy-mm-dd')), array(
        'prepend' => '<i class="icon-calendar"></i>',
    )); ?>
    <?= $form->dropDownListRow($model, 'status', $model::statuses()); ?>
    <?= $form->textFieldRow($model, 'meta_title', array('class' => 'span8')); ?>        
    <?= $form->textAreaRow($model, 'meta_keywords', array('class' => 'span8', 'rows' => 5)); ?>
    <?= $form->textAreaRow($model, 'meta_description', array('class' => 'span8', 'rows' => 5)); ?> 
    
</fieldset>
<?= TbHtml::formActions(array(
    TbHtml::submitButton($model->getIsNewRecord() ? 'Create' : 'Save', array('color' => TbHtml::BUTTON_COLOR_PRIMARY)),
    TbHtml::submitButton($model->getIsNewRecord() ? 'Create and continue' : 'Save and continue', array('name' => 'continue', 'color' => TbHtml::BUTTON_COLOR_SUCCESS))
))?>


<?php $this->endWidget(); ?>
