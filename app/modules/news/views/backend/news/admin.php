<?php
$this->pageTitle= 'Manage news';

$this->breadcrumbs=array(
	'News',
);
?>

<h1>Manage news</h1>

<?= TbHtml::link('Create new news', array('create'), array('class' => 'btn')); ?>
<br />

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => $model->getGridId(),
	'dataProvider' => $model->search(),
	'filter' => $model,
    'template' => "{items}\n{pager}",
	'columns' => array(
		array(
			'name' => 'title',
			'type' => 'raw',
			'value' => 'CHtml::encode($data->title)',
			'sortable' => false,
		),
		array(
			'name' => 'status',
			'value' => 'News::statuses($data->status)',
            'filter' => News::statuses()
		),
		array(
			'name'=>'published_at',
			'filter' => false,
			'sortable' => false,
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'deleteConfirmation' => 'Are you sure to delete this news?',
			'template' => '{update} {delete}',
		),
	),
)); 
?>