<?php
$this->pageTitle = 'Create news';
$this->breadcrumbs=array(
	'Manage news' => array('admin'),
	'Create News',
);

$this->renderPartial('_form', array('model' => $model)); 
?>