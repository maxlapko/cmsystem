<?php

class BlogModule extends EWebModule
{

    public $defaultController = 'posts';

    public function init()
    {
        parent::init();
        Yii::app()->setComponents(array(
            'image' => array(
                'presets' => array(
                    'post_preview' => array(
                        'thumb' => array('width'  => 100, 'height' => 100)
                    ),
                )
            )
        ));

        $this->setImport(array(
            'blog.models.*',
        ));
    }
    
    public function getArchive()
    {
        return Post::getArchive();
    }
    
    public function getArchiveDate($date)
    {
        $tokens = explode('-', $date);
        if (count($tokens) !== 2) {
            return '';
        }
        return Yii::app()->getLocale()->getMonthName((int) $tokens[1], 'wide', true) . ' ' . $tokens[0];
    }
    
    public function getStringEntity()
    {
        return 'post';
    }
    
    public function search()
    {
        return array(
            'sql' => '
                SELECT {select}
                FROM posts
                INNER JOIN post_i18ns
                    ON post_i18ns.parent_id = posts.id AND post_i18ns.lang_id = :lang 
                WHERE posts.`status` = ' . Post::STATUS_PUBLISHED . ' 
                    AND post_i18ns.title LIKE :q OR post_i18ns.content LIKE :q',
            'select' => 'posts.id, "Post" AS model_type, posts.published_at AS `sort_at`'
        );
    }

}
