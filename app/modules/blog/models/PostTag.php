<?php

class PostTag extends EActiveRecord
{
    /**
     * The followings are the available columns in table 'tbl_tag':
     * @var integer $id
     * @var string $name
     * @var integer $type
     * @var integer $frequency
     */

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'post_tags';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('frequency, type', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 128),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'        => 'Id',
            'name'      => 'Name',
            'frequency' => 'Frequency',
        );
    }

    /**
     * Returns tag names and their corresponding weights.
     * Only the tags with the top weights will be returned.
     * @param integer $limit the maximum number of tags that should be returned
     * @param integer $type
     * @return array weights indexed by tag names.
     */
    public function findTagWeights($limit = 20, $type = null)
    {
        return $this->findAll(array(
            'condition' => 'frequency > 0 ' . (($type === null) ? '' : (' AND `type` = ' . $type)),
            'order'     => 'frequency DESC',
            'limit'     => $limit,
        ));
    }

    /**
     * Suggests a list of existing tags matching the specified keyword.
     * @param string the keyword to be matched
     * @param integer maximum number of tags to be returned
     * @return array list of matching tag names
     */
    public static function suggest($keyword, $limit = 20)
    {
        $tags = self::model()->findAll(array(
            'condition' => 'name LIKE :keyword',
            'select' => 'id, name',
            'order'     => 'frequency DESC, name',
            'limit'     => $limit,
            'params'    => array(
                ':keyword' => '%' . strtr($keyword, array('%'    => '\%', '_'    => '\_', '\\'   => '\\\\')) . '%',
            ),
        ));
        $names = array();
        foreach ($tags as $tag) {
            $names[] = array('id' => $tag->id, 'text' => $tag->name);
        }
        return $names;
    }
    
    public function getUrl()
    {
        return Yii::app()->createUrl('/post/index', array(
            'tag' => $this->id . '-' . $this->name,
        ));
    }

}