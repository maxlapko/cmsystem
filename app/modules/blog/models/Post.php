<?php

class Post extends I18nActiveRecord
{
    const STATUS_DRAFT = '0';
    const STATUS_PUBLISHED = '1';
    const STATUS_ARCHIVED = '2';

    public $id;
    public $created_at;
    public $updated_at;
    public $published_at;
    public $slug;
    public $image;
    public $category_id;
    public $searchTitle;
    
    public $i18nModel = 'PostI18n';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'posts';
    }

    public function rules()
    {
        return array(
            array(
                'image', 'ext.components.image_processor.MImageValidator', 'allowEmpty' => true,
                'types' => array('jpg', 'png', 'jpeg'), 'maxSize' => 5 * 1024 * 1024, 
//                'minWidth' => 100, 'minHeight' => 100
            ),
            array('stringTags, status, published_at, category_id, translateattrs, slug', 'safe'),
            array('searchTitle, status', 'safe', 'on' => 'search'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'               => 'ID',
            'slug'             => 'Slug',
            'stringTags'       => 'Tags',
            'image'            => 'Image',
            'created_at'       => 'Create date',
            'updated_at'       => 'Update date',
            'published_at'     => 'Publish date',
            'status'           => 'Status',
            'category_id'      => 'Category',
        );
    }
    
    public function relations()
    {
        return array(
            'tags' => array(self::MANY_MANY, 'PostTag', 'posts_tags(post_id, tag_id)'),
            'i18ns' => array(self::HAS_MANY, 'PostI18n', 'parent_id', 'index' => 'lang_id')
        );
    }
    
    public function scopes()
    {
        $a = $this->getTableAlias(false, false);
        return array(
            'published' => array(
                'condition' => "$a.`status` = " . self::STATUS_PUBLISHED . " AND $a.`published_at` >= " . date('Y-m-d')
            ),
            'resent' => array('order' => "$a.`published_at` DESC"),
        );
    }
    
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.*';
        $criteria->join = 'INNER JOIN post_i18ns ON post_i18ns.parent_id = t.id';
        $criteria->group = 't.id';
        $criteria->compare('post_i18ns.title', $this->searchTitle, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('slug', $this->slug, true);

        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'sort'       => array(
                'defaultOrder' => 'published_at DESC',
            ),
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));
    }

    public function behaviors()
    {
        return array(
            'AutoTimestampBehavior' => array(
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created_at',
                'updateAttribute'   => 'updated_at',
                'setUpdateOnCreate' => true
            ),
            'slugable'          => array(
                'class'     => 'ext.behaviors.SlugBehavior',
                'scenarios' => array('insert'),
                'sourceAttribute' => function($owner) {
                    return trim($owner->slug) === '' ? 
                        $owner->getI18n('title', false, Yii::app()->getModule('languages')->getDefault()) :
                        $owner->slug;
                }
            ),
            'taggable' => array(
                'class'                   => 'ext.behaviors.taggable.ETaggableBehavior',
                'tagTable'                => 'post_tags',
                'tagBindingTable'         => 'posts_tags',
                'modelTableFk'            => 'post_id',
                'tagTablePk'              => 'id',
                'tagTableName'            => 'name',
                'tagTableCount'           => 'frequency',
                'tagBindingTableTagId'    => 'tag_id',
                'cacheID'                 => false,
                'createTagsAutomatically' => true,
//                'scope' => array(
//                    'condition' => ' t.`type` = :type',
//                    'params' => array(':type' => $this->type),
//                ),
//                'insertValues' => array(
//                    'type' => $this->type,
//                ),
            ),
            'mImage' => array(
                'class'          => 'ext.components.image_processor.MImageBehavior',
                'imageProcessor' => 'image' // image processor component name 
            )
        );
    }
    
    public function setTranslateAttrs($i18ns)
    {
        $this->setNestedRelation('i18ns', $i18ns);
        $this->clearDirtyAttributes($this->i18ns);
    }
    
    public function getUrl()
    {
        $url = Yii::app()->createUrl('/blog/posts/show', array('slug' => $this->slug));
        if (!HApp::isFront()) {
            $url = strtr($url, array(param('backend.link') => ''));
        }
        return $url;
    }
    
    public function getEditLink($params = array())
    {
        $params['id'] = $this->id;
        return (HApp::isFront() ? param('backend.link') : '') . Yii::app()->createUrl('/blog/posts/update', $params);
    }


    public function archive($date)
    {
        $criteria = $this->getDbCriteria();
        $criteria->addCondition('`published_at` LIKE :published');
        $criteria->params[':published'] = $date . '%';
        return $this;
    }
    
    public function byCategory($code)
    {
        $criteria = $this->getDbCriteria();
        $criteria->addCondition('`category_id` = :category');
        $criteria->params[':category'] = $code;
        return $this;
    }
    
    public function searching($q)
    {
        $criteria = $this->getDbCriteria();
        $criteria->join .= ' 
            INNER JOIN post_i18ns 
                ON post_i18ns.parent_id = t.id AND post_i18ns.lang_id = "' . Yii::app()->getLanguage() . '" ';
        $criteria->addCondition('post_i18ns.title LIKE :q OR post_i18ns.content LIKE :q');
        $criteria->params[':q'] = '%' . strtr($q, array('%'=>'\%', '_'=>'\_', '\\'=>'\\\\')) . '%';
    }
    
    public static function getArchive()
    {
        return Yii::app()->getDb()->createCommand('
            SELECT DATE_FORMAT(published_at, "%Y-%m") AS archive_date, COUNT(*) AS post_count
            FROM posts
            WHERE 
                posts.`status` = ' . self::STATUS_PUBLISHED . '
            GROUP BY DATE_FORMAT(published_at, "%Y-%m")
            ORDER BY archive_date DESC'
        )->queryAll();
    }

    public function getStringTags()
    {
        return implode(', ', $this->getTags());
    }
    
    public function setStringTags($tags)
    {
        return $this->setTags($tags);
    }
    
    public function getCategory()
    {
        return Yii::app()->getModule('categories')->getItemByCode($this->category_id);
    }
    
    public static function getPostIds()
    {
        static $ids;
        if ($ids === null) {
            if (($ids = Yii::app()->getCache()->get('post_ids')) === false) {
                $scopes = self::model()->scopes();
                $ids = Yii::app()->getDb()->createCommand('
                    SELECT t.id FROM posts AS t 
                    WHERE ' . $scopes['published']['condition'] . ' 
                    ORDER BY ' . $scopes['resent']['order']
                )->queryColumn();
            }
        }
        return $ids;
    }
    
    public function getNextPost($lang = null)
    {
        $ids = self::getPostIds();
        if (($i = array_search($this->id, $ids)) !== false && isset($ids[$i + 1])) {
            return self::model()->resetScope()->withLang($lang)->findByPk($ids[$i + 1]);
        }
        return null;
    }
    
    public function getPrevPost($lang = null)
    {
        $ids = self::getPostIds();
        if (($i = array_search($this->id, $ids)) !== false && isset($ids[$i - 1])) {
            return self::model()->resetScope()->withLang($lang)->findByPk($ids[$i - 1]);
        }
        return null;
    }
    
    protected function beforeSave()
    {
        $this->published_at = date('Y-m-d', strtotime($this->published_at));
        return parent::beforeSave();
    }

    protected function afterFind()
    {
        $this->published_at = date('Y-m-d', strtotime($this->published_at));
        return parent::afterFind();
    }
    
    public static function statuses($status = null)
    {
        static $statuses = array(
            self::STATUS_DRAFT => 'Draft',
            self::STATUS_PUBLISHED => 'Published',
            self::STATUS_ARCHIVED => 'Archived',
        );
        return $status === null ? $statuses : $statuses[$status];
    }

}