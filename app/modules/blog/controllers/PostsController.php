<?php

class PostsController extends FrontendController
{
    public $modelName = 'Post';
    
    public function actionArchive($date)
    {
        $dataProvider = new CActiveDataProvider(Post::model()->published()->withLang()->archive($date));
        $this->render('archive', compact('dataProvider', 'date'));
    }
    
    public function actionCategory($code)
    {
        $category = Yii::app()->getModule('categories')->getByCode($code, true);        
        $dataProvider = new CActiveDataProvider(Post::model()->published()->withLang()->byCategory($code));
        $this->render('category', compact('dataProvider', 'category'));
    }
    
    public function actionIndex()
    {
        $this->aMenu = array(
            array('label' => 'Manage blog', 'url' => '/backend.php/blog/posts/admin'),
            array('label' => 'Create a new post', 'url' => '/backend.php/blog/posts/create'),
        );
        $dataProvider = new CActiveDataProvider(Post::model()->published()->withLang());
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }
}