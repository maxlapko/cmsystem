<?php
$this->breadcrumbs=array(
    'Posts' => array('/blog/pages/index'),
    $model->getI18n('title')
);
?>

<div class="post-item">
    <p class="date"><?= $model->published_at; ?></p>
    <p class="title"><h2><?= CHtml::encode($model->getI18n('title')); ?></h2></p>
    <div class="desc">
        <?= $model->getI18n('content'); ?>
    </div>
</div>