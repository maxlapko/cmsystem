<?php
$this->breadcrumbs = array(
    'Posts',
);
?>
<h1>Posts</h1>
<?php
$this->widget('application.modules.blog.widgets.ArchiveWidget');
$this->widget('application.modules.blog.widgets.BlogCategoryWidget');

$this->renderPartial('_list', compact('dataProvider'));
?>