<?php
$this->pageTitle= 'Manage posts';

$this->breadcrumbs=array(
    'Posts',
);
?>

<h1>Manage posts</h1>

<?= TbHtml::link('Create new post', array('create'), array('class' => 'btn')); ?>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => $model->getGridId(),
    'dataProvider' => $model->search(),
    'filter' => $model,
    'template' => "{items}\n{pager}",
    'columns' => array(
        array(
            'header'   => 'Title',
            'type'     => 'raw',
            'name'     => 'searchTitle',
            'value'    => 'CHtml::encode($data->getI18n("title"))',
            'sortable' => false,
        ),
        array(
            'name' => 'slug',
            'value' => '$data->slug',
            'sortable' => false,
        ),
        array(
            'name' => 'status',
            'value' => 'Post::statuses($data->status)',
            'filter' => Post::statuses()
        ),
        array(
            'name' => 'published_at',
            'filter' => false,
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {update} {delete}',
            'deleteConfirmation' => 'Are you sure to delete this post?',
            'viewButtonUrl' => '$data->getUrl(true)'
        ),
    ),
)); 
?>