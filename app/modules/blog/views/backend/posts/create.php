<?php
$this->pageTitle = 'Create post';
$this->breadcrumbs=array(
    'Manage posts' => array('admin'),
    'Create post',
);

$this->renderPartial('_form', array('model' => $model)); 
?>