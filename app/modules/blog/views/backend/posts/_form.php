<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
    $model->published_at = $model->published_at ? $model->published_at : date('Y-m-d');
?>
<fieldset>
    <?php $this->renderPartial('_i18ns', array('model' => $model, 'form' => $form)) ?>
    <?= $form->textFieldRow($model, 'slug', array('class' => 'span8')); ?>        
    <?= $form->select2Row($model, 'stringTags', array(
        'asDropDownList' => false, 
        'options' => array(            
            'width' => '618px',
            'tags' => $model->getAllTags(),
            'placeholder' => '',
            'tokenSeparators' => array(','),
            'minimumInputLength' => 2,
        ))
    );?>
    <?= $form->dropDownListRow(
        $model, 
        'category_id', 
        Yii::app()->getModule('categories')->getListData('blog_category', false), 
        array('encode' => false, 'prompt' => 'Select category ...')
    ); ?>
    <?= $form->datepickerRow($model, 'published_at', array('options' => array('format' => 'yyyy-mm-dd')), array(
        'prepend' => '<i class="icon-calendar"></i>',
    )); ?>
    <?= $form->fileFieldRow($model, 'image', array(), array('hint' => $model->image ? CHtml::image($model->getImageUrl('image', 'image_preview'), '') : '')); ?>
    <?= $form->dropDownListRow($model, 'status', $model::statuses()); ?>
    
</fieldset>
<?= HApp::saveButtons($model) ?>

<?php $this->endWidget(); ?>
