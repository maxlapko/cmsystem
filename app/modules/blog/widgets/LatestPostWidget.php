<?php

class LatestPostWidget extends CWidget
{
    public $limit = 5;
    public $view = 'latest_post';
    
    public function run()
    {
        Yii::app()->getModule('blog');
        $posts = Post::model()->resetScope()->resent()->published()->withLang()->findAll(array(
            'limit' => $this->limit
        ));
        $this->render($this->view, compact('posts'));
    }
}