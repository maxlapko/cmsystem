<?php

class BlogCategoryWidget extends CWidget
{
    public $code = 'blog_category';
    
    public function run()
    {
        $categories = Yii::app()->getModule('categories')->getListCategory($this->code, false, false);
        $this->render('categories', compact('categories'));
    }
}