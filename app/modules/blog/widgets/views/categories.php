<h2>Blog categories</h2>
<ul>
<?php foreach ($categories as $cat) : ?>
    <li><?= CHtml::link(CHtml::encode($cat->getI18n('name')), array('/blog/posts/category', 'code' => $cat->code))?></li>
<?php endforeach; ?>         
</ul>