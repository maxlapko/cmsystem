<?php 
$module = Yii::app()->getModule('blog');
?>
<h2>Archive</h2>
<ul>
<?php foreach ($archive as $info) : ?>
    <li><?= CHtml::link($module->getArchiveDate($info['archive_date']) . " ({$info['post_count']})", array('/blog/posts/archive', 'date' => $info['archive_date']))?></li>
<?php endforeach; ?>         
</ul>