<?php

class m140204_120757_create_dynamic_params extends CDbMigration
{

    public function up()
    {
        $sql = <<< EOD
DROP TABLE IF EXISTS `page_dynamic_params`;
CREATE TABLE `page_dynamic_params` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type` varchar(255) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_idx` (`entity_type`,`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
EOD;
        $this->execute($sql);
    }

    public function down()
    {
        $this->dropTable('page_dynamic_params');
    }
}