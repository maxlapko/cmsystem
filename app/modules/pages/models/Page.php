<?php

class Page extends I18nActiveRecord
{
    const DRAFT = '0';
    const PUBLISHED = '1';
    const ARCHIVED = '2';

    public $id;
    public $created_at;
    public $updated_at;
    public $slug;
    public $template;
    public $image;
    public $type;
    public $category_id;
    public $searchTitle;
    
    public $i18nModel = 'PageI18n';
    
    public $dynamicParamsTable = 'page_dynamic_params';

    /**
     * Options for dynamic attributes
     * key - attribute name
     * value - array of
     *  saveEmpty - flag for allowing to save empty value to db 
     * 
     * @var array 
     */
    protected static $_dynamicOptions = array();
    protected $_dynamicAttributes;
    protected $_dynamicNeedSave = false;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'pages';
    }

    public function rules()
    {
        $rules = array(
            array('template', 'required'),
            array('slug, status, category_id', 'safe'),
            array('category_id, status, searchTitle', 'safe', 'on' => 'search'),
            array('translateattrs', 'safe'),
            array(
                'image', 'ext.components.image_processor.MImageValidator', 'allowEmpty' => true,
                'types' => array('jpg', 'png', 'jpeg'), 'maxSize' => 5 * 1024 * 1024, 
//                'minWidth' => 10, 'minHeight' => 10
            ),
        );
        if (!empty(static::$_dynamicOptions)) {
            $rules[] = array(implode(',', array_keys(static::$_dynamicOptions)), 'safe');
        }
        return $rules;
    }

    public function attributeLabels()
    {
        return array(
            'id'               => 'ID',
            'slug'             => 'Slug',
            'category_id'      => 'Category',
            'created_at'       => 'Create date',
            'updated_at'       => 'Update date',
            'status'           => 'Status',           
        );
    }
    
    public function relations()
    {
        return array(
            'i18ns' => array(self::HAS_MANY, 'PageI18n', 'parent_id', 'index' => 'lang_id')
        );
    }
    
    public function scopes()
    {
        $a = $this->getTableAlias(false, false);
        return array(
            'published' => array('condition' => "$a.`status` = " . self::PUBLISHED),
            'ordered' => array('order' => "$a.`order` ASC"),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        
        $criteria->select = 't.*';
        $criteria->join = 'INNER JOIN page_i18ns ON page_i18ns.parent_id = t.id';
        $criteria->group = 't.id';
        $criteria->compare('page_i18ns.title', $this->searchTitle, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('category_id', $this->category_id);

        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'sort'       => array(
                'defaultOrder' => '`created_at` DESC',
            ),
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));
    }

    public function behaviors()
    {
        return array(
            'AutoTimestampBehavior' => array(
                'class'           => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at',
                'setUpdateOnCreate' => true
            ),
            'slugable' => array(
                'class' => 'ext.behaviors.SlugBehavior',
                'scenarios' => array('insert'),
                'sourceAttribute' => function($owner) {
                    return trim($owner->slug) === '' ? 
                        $owner->getI18n('title', false, Yii::app()->getModule('languages')->getDefault()) :
                        $owner->slug;
                }
            ),
            'mImage' => array(
                'class'          => 'ext.components.image_processor.MImageBehavior',
                'imageProcessor' => 'image' // image processor component name 
            )
        );
    }
    
    public function setTranslateAttrs($i18ns)
    {
        $this->setNestedRelation('i18ns', $i18ns);
        $this->clearDirtyAttributes($this->i18ns);
    }
    
    public function getUrl()
    {
        $url = Yii::app()->createUrl('/pages/pages/show', array('slug' => $this->slug));
        if (!HApp::isFront()) {
            $url = strtr($url, array(param('backend.link') => ''));
        }
        return $url;
    }
    
    public function getEditLink($params = array())
    {
        $params['id'] = $this->id;
        return (HApp::isFront() ? param('backend.link') : '') . Yii::app()->createUrl('/pages/pages/update', $params);
    }
    
    public function byCategory($category)
    {
        $criteria = $this->getDbCriteria();
        $a = $this->getTableAlias(false, false);
        $criteria->addCondition("$a.category_id = :category_id");
        $criteria->params[':category_id'] = $category;
        return $this;
    }
    
    public function getCategory()
    {
        return Yii::app()->getModule('categories')->getItemByCode($this->category_id);
    }
    
    public static function statuses($status = null)
    {
        static $statuses = array(
            self::DRAFT => 'Draft',
            self::PUBLISHED => 'Published',
            self::ARCHIVED => 'Archived',
        );
        return $status === null ? $statuses : $statuses[$status];
    }
    
    public static function getBySlug($slug)
    {
        return self::model()->findByAttributes(array('slug' => $slug));
    }
    
    /**
     * @return Image[]
     */
    public function getImages()
    {
        return Yii::app()->getModule('images')->getImages($this);
    }
    
    public static function getIds($category)
    {
        static $ids;
        if (empty($category)) {
            $category = 'empty';
        }
        if (!isset($ids[$category])) {
            $key = "page-$category-ids";
            if (($ids[$category] = Yii::app()->getCache()->get($key)) === false) {
                $ids[$category] = Yii::app()->getDb()->createCommand(
                    'SELECT t.id FROM `pages` AS t WHERE category_id = :category_id'
                )->queryColumn(array(':category_id' => $category));
                Yii::app()->getCache()->set($key, $ids[$category]);
            }
        }
        return $ids[$category];
    }
    
    public function getNext($lang = null)
    {
        $ids = self::getIds($this->category_id);
        if (($i = array_search($this->id, $ids)) !== false && isset($ids[$i + 1])) {
            return self::model()->resetScope()->withLang($lang)->findByPk($ids[$i + 1]);
        }
        return null;
    }
    
    public function getPrev($lang = null)
    {
        $ids = self::getIds($this->category_id);
        if (($i = array_search($this->id, $ids)) !== false && isset($ids[$i - 1])) {
            return self::model()->resetScope()->withLang($lang)->findByPk($ids[$i - 1]);
        }
        return null;
    }
    
    
    public function getDynamicParams()
    {
        if ($this->_dynamicAttributes === null) {
            $this->_dynamicAttributes = array();
            if (!$this->getIsNewRecord()) {
                $criteria = new CDbCriteria;
                $criteria->params = array(':type' => get_class($this), ':id' => $this->id);
                $criteria->condition = 'entity_type = :type AND entity_id = :id';
                $params = $this->_dynamicAttributes = Yii::app()->getDb()->getCommandBuilder()
                    ->createFindCommand($this->dynamicParamsTable, $criteria)
                    ->queryAll();
                $this->_dynamicAttributes = HArray::collect($params, 'key', 'value');
            }
        }
        return $this->_dynamicAttributes;
    }
    
    /**
     * @param array $params
     * @return \Page
     */
    public function byParams($params)
    {
        $criteria = $this->getDbCriteria();
        $i = 0;
        $type = get_class($this);
        foreach ($params as $key => $value) {
            $i++;
            if (isset(static::$_dynamicOptions[$key]) && !empty($value)) {
                $criteria->join .= "
                    INNER JOIN `{$this->dynamicParamsTable}` AS do$i
                        ON t.id = do$i.entity_id AND do$i.entity_type = '$type'
                        AND do$i.key = '$key' AND do$i.value = :do$i ";
                $criteria->params[":do$i"] = $value;
            }
        }
        return $this;
    }
    
    protected function afterSave()
    {
        parent::afterSave();
        $category = $this->category_id;
        if (empty($category)) {
            $category = 'empty';
        }
        if (!empty(static::$_dynamicOptions)) {
            $this->_saveDynamicParams();
        }
        
        $cache = Yii::app()->getCache();
        $cache->delete("page-$category-ids");
        $cache->delete('page-' . $this->getOldValue('category_id') . '-ids');
    }
    
    
    protected function _saveDynamicParams()
    {
        $this->_removeDynamicParams();
        if (empty($this->_dynamicAttributes)) return;
        $type = get_class($this);
        $data = array();
        foreach ($this->_dynamicAttributes as $key => $value) {
            $data[] = array(
                'entity_id' => $this->id,
                'entity_type' => $type,
                'key' => $key,
                'value' => $value,
            );
        }
        
        return Yii::app()->getDb()->getCommandBuilder()
            ->createMultipleInsertCommand($this->dynamicParamsTable, $data)
            ->execute();
    }
    
    protected function _removeDynamicParams()
    {
        if ($this->getIsNewRecord()) return;
        $criteria = array(
            'condition' => 'entity_type = :type AND entity_id = :id ',
            'params' => array(':type' => get_class($this), ':id' => $this->id),
        );
        return Yii::app()->getDb()->getCommandBuilder()
            ->createDeleteCommand($this->dynamicParamsTable, new CDbCriteria($criteria))
            ->execute();
    }
    
    

    public function __get($name)
    {
        $params = $this->getDynamicParams();
        if (isset($params[$name])) {
            return $params[$name];
        } elseif (isset(static::$_dynamicOptions[$name])) {
            return null;
        }
        return parent::__get($name);
    }
    
    public function __set($name, $value)
    {
        if (isset(static::$_dynamicOptions[$name])) {
            if (!empty($value) || !empty(static::$_dynamicOptions[$name]['saveEmpty'])) { //save empty value or not
                $this->_dynamicAttributes[$name] = $value;
            }
            return;
        }
        parent::__set($name, $value);
    }
}