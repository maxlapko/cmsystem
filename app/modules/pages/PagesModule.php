<?php

class PagesModule extends EWebModule 
{
    public $defaultController = 'pages';
    
    public function init()
    {
        parent::init();
        
        $this->setImport(array(
            'pages.models.*',
        ));
    }
    
    public function getStringEntity()
    {
        return 'page';
    }
    
    public function search()
    {
        return array(
            'sql' => '
                SELECT {select}
                FROM pages
                INNER JOIN page_i18ns
                    ON page_i18ns.parent_id = pages.id AND page_i18ns.lang_id = :lang
                WHERE pages.`status` = ' . Page::PUBLISHED . ' 
                    AND page_i18ns.title LIKE :q OR page_i18ns.content LIKE :q',
            'select' => 'pages.id, "Page" AS model_type, pages.updated_at AS `sort_at`'
        );
    }
}
