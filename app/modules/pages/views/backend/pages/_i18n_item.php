<?php $prefix = 'Page[translateattrs][' . $lang['id'] . ']'; ?>
<?= CHtml::hiddenField($prefix . '[lang_id]', $lang['id'])?>
<?= $form->textFieldRow($model, 'title', array('class' => 'span8', 'id' => "label-title-{$lang['id']}", 'name' => $prefix . '[title]')); ?>        
<?= $form->redactorRow($model, 'content', array(
    'htmlOptions' => array(
        'id' => "label-content-{$lang['id']}", 
        'name' => $prefix . '[content]',
    ),
    'editorOptions' => array(
        'minHeight' => 100,
        'imageUpload' => $this->createUrl('/images/images/redactorupload'),
        'imageGetJson' => $this->createUrl('/images/images/redactorindex'),
    ),
)); ?>
<?= $form->redactorRow($model, 'short_content', array(
    'htmlOptions' => array(
        'id' => "label-short-content-{$lang['id']}", 
        'name' => $prefix . '[short_content]',
    ),
)); ?>
<?= $form->textFieldRow($model, 'meta_title', array('class' => 'span8', 'id' => "label-meta_title-{$lang['id']}", 'name' => $prefix . '[meta_title]')); ?>        
<?= $form->textAreaRow($model, 'meta_keywords', array('class' => 'span8', 'rows' => 5, 'id' => "label-meta_keywords-{$lang['id']}", 'name' => $prefix . '[meta_keywords]')); ?>
<?= $form->textAreaRow($model, 'meta_description', array('class' => 'span8', 'rows' => 5, 'id' => "label-meta_description-{$lang['id']}", 'name' => $prefix . '[meta_description]')); ?>