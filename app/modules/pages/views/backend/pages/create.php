<?php
$this->pageTitle = 'Create page';
$this->breadcrumbs=array(
	'Manage pages' => array('admin'),
	'Create page',
);
$model->template = $model->template ?: 'view';

$this->renderPartial('_form', array('model' => $model)); 
?>