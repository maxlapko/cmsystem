<?php
$this->pageTitle= 'Manage pages';

$this->breadcrumbs=array(
	'Pages',
);
?>

<h1>Manage pages</h1>

<?= TbHtml::link('Create new page', array('create'), array('class' => 'btn')); ?>
<br />

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id'           => $model->getGridId(),
    'dataProvider' => $model->search(),
    'filter'       => $model,
    'template'     => "{items}\n{pager}",
    'columns'      => array(
        array(
            'header'   => 'Title',
            'type'     => 'raw',
            'name'     => 'searchTitle',
            'value'    => 'CHtml::encode($data->getI18n("title"))',
            'sortable' => false,
        ),
        array(
            'name' => 'category_id',
            'filter' => Yii::app()->getModule('categories')->getListData('page_category', false),
            'filterInputOptions' => array('encode' => false)
        ),
        array(
            'name'   => 'status',
            'value'  => 'Page::statuses($data->status)',
            'filter' => Page::statuses()
        ),
        array(
            'name'     => 'created_at',
            'filter'   => false,
            'sortable' => false,
        ),
        array(
            'class'              => 'bootstrap.widgets.TbButtonColumn',
            'deleteConfirmation' => 'Are you sure to delete this page?',
            'template' => '{view} {update} {delete}',
            'viewButtonUrl' => '$data->getUrl(true)'
        ),
    ),
)); 
?>