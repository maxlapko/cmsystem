<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
?>
<fieldset>    
    <?php $this->renderPartial('_i18ns', array('model' => $model, 'form' => $form)) ?>       
    <?= $form->textFieldRow($model, 'slug', array('class' => 'span8')); ?>       
    <?= $form->dropDownListRow($model, 'template', $this->getTemplates()); ?>       
    <?= $form->dropDownListRow(
        $model, 
        'category_id', 
        Yii::app()->getModule('categories')->getListData('page_category', false), 
        array('encode' => false, 'prompt' => 'Select category')
    ); ?>
    <?= $form->fileFieldRow($model, 'image', array(), array('hint' => $model->image ? CHtml::image($model->getImageUrl('image', 'image_preview'), '') : '')); ?>
    <?= $form->dropDownListRow($model, 'status', $model::statuses()); ?>
    
</fieldset>
<?= HApp::saveButtons($model) ?>


<?php $this->endWidget(); ?>
