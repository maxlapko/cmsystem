<?php

class PagesController extends FrontendController
{
    public $modelName = 'Page';
    
    public function actionShow($slug)
    {
        $model = $this->loadModelBySlug('Page', $slug);
        HApp::attachMetaParams($model);
        $this->render($model->template, array('model' => $model));
    }
}