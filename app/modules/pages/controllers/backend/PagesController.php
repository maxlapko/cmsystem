<?php

class PagesController extends BackendController
{
    public $modelName = 'Page';
    public $redirectTo = array('admin');
    public $defaultAction = 'admin';
    
    public function getTemplates()
    {
        $theme = require Yii::getPathOfAlias('application.config') . '/frontend.php';
        $theme = isset($theme['theme']) ? ($theme['theme'] . '/') : '';
        $dir =  Yii::app()->getThemeManager()->getBasePath() . '/' . $theme . 'views/' . $this->getModule()->getId() . '/' . $this->getId();
        $templates = array();
        foreach (CFileHelper::findFiles($dir) as $file) {
            $basename = basename($file);
            if (preg_match('/^[^_].*v?iew\.php$/iU', $basename)) {                
                $code = str_replace('.php', '', $basename);
                $templates[$code] = $this->_getTemplateName($file, $code);
            }
        }
        return $templates;
    }
    
    protected function _getTemplateName($file, $code) 
    {
        $templateData = implode('', file($file));
        if (preg_match('|Template Name:(.*)$|mi', $templateData, $name)) {
            return sprintf('%s Page Template', trim(preg_replace("/\s*(?:\*\/|\?>).*/", '', $name[1])));
        }
        return $code;
    }
    
}