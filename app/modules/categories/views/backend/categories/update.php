<?php
/* @var $this CategoriesController */
/* @var $model Category */

$this->breadcrumbs = array(
    'Categories' => array('admin'),
    'Update',
);
?>

<h1>Update category list <?= CHtml::encode('"' . $model->name . '"'); ?></h1>

<?= $this->renderPartial('_form', array('model' => $model)); ?>

