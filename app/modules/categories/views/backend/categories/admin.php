<?php
/* @var $this CategoriesController */
/* @var $model Category */

$this->breadcrumbs = array(
	'Categories' => array('admin'),
	'Manage',
);
?>

<h1>Manage category list</h1>

<?= TbHtml::link('Create new category list', array('create'), array('class' => 'btn')); ?>
<br />

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider' => $model->search(),
    'template' => "{items}\n{pager}",
    'id' => $model->getGridId(),
    'columns' => array(
        array(
            'class'=>'bootstrap.widgets.TbRelationalColumn',
            'name' => 'code',
            'url' => $this->createUrl('/categories/items/index'),
            'afterAjaxUpdate' => 'js:function(tr, rowid, data) {}',
            'sortable' => false,
        ),
        array('name' => 'name', 'sortable' => false),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template'=>'{update} {delete}',
        )
    ),
)); 
Yii::app()->getClientScript()->registerCoreScript('jquery.ui');
?>
