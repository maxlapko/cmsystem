<?php

class CategoryItem extends I18nActiveRecord 
{
    public $parent_id;
    public $i18nModel = 'CategoryItemI18n';

    public static function model($className = __CLASS__) 
    {
        return parent::model($className);
    }

    public function tableName() 
    {
        return 'category_items';
    }

    public function rules() 
    {
        return array(
            array('parent_id', 'required'),
            array('url, code', 'length', 'max' => 255),
            array('translateattrs, code', 'safe'),
            array('id, url', 'safe', 'on' => 'search'),
        );
    }
    
    public function behaviors()
    {
        return array(
            'nestedSetBehavior' => array(
                'class' => 'ext.behaviors.NestedSetBehavior',
                'hasManyRoots' => true,
                'rootAttribute' => 'root',
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'levelAttribute' => 'level',
            ),
            'slugable' => array( 
                'class'     => 'ext.behaviors.SlugBehavior',
                'scenarios' => array('insert'),
                'sourceAttribute' => function($owner) {
                    return $owner->getI18n('name', false, Yii::app()->getModule('languages')->getDefault());
                },
                'slugAttribute' => 'code'
            ),
            'CTimestampBehavior' => array(
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created_at',
                'updateAttribute'   => null
            )
        );
    }
    
    public function relations()
    {
        return array(
            'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
            'i18ns' => array(self::HAS_MANY, 'CategoryItemI18n', 'parent_id', 'index' => 'lang_id')
        );
    }

    public function attributeLabels() 
    {
        return array(
            'id' => 'ID',
            'url' => 'Url',
            'code' => 'Code',
            'parent_id' => 'Parent',
        );
    }
    
    public function setTranslateAttrs($i18ns)
    {
        $this->setNestedRelation('i18ns', $i18ns);
        $this->clearDirtyAttributes($this->i18ns);
    }

    public function search() 
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('code', $this->code, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public static function getByCode($code, $lang = null)
    {
        static $categories = array();
        if (!array_key_exists($code, $categories)) {
            $categories[$code] = self::model()->withLang($lang)->findByAttributes(array('code' => $code));
        }
        return $categories[$code];
    }
    
    public static function getByEntity($model, $parentCode = null)
    {
        $criteria = array(
            'scopes' => array('withLang'),
            'join' => '
                INNER JOIN `categories_entities` AS ce ON ce.item_id = t.code
                    AND ce.entity_type = :type AND ce.entity_id = :id ',
            'params' => array(':type' => get_class($model), ':id' => $model->id)
        );
        if ($parentCode !== null) {
            $criteria['join'] .= 'AND ce.parent_code = :code ';
            $criteria['params'][':code'] = $parentCode;
        }        
        return self::model()->findAll($criteria);
    }
    
    /**
     * @param CActiveRecord $model
     * @param array $ids
     * @param string $parentCode
     */
    public static function setForEntity($model, $ids, $parentCode = null)
    {
        self::removeByEntity($model, $parentCode);
        if (empty($ids)) {
            return;
        }
        $type = get_class($model);
        $data = array();
        foreach ($ids as $id) {
            $data[] = array(
                'item_id' => $id,
                'entity_id' => $model->id,
                'entity_type' => $type,
                'parent_code' => $parentCode,
            );
        }
        
        return Yii::app()->getDb()->getCommandBuilder()->createMultipleInsertCommand('categories_entities', $data)->execute();
    }
    
    public static function removeByEntity($model, $parentCode = null)
    {
        $criteria = array(
            'condition' => 'entity_type = :type AND entity_id = :id ',
            'params' => array(':type' => get_class($model), ':id' => $model->id),
        );
        if ($parentCode !== null) {
            $criteria['condition'] .= ' AND parent_code = :code ';
            $criteria['params'][':code'] = $parentCode;
        }
        return Yii::app()->getDb()->getCommandBuilder()
            ->createDeleteCommand('categories_entities', new CDbCriteria($criteria))
            ->execute();
    }
    
    public function populateParent()
    {
        $this->parent_id = $this->parent()->find()->id;
    }
}