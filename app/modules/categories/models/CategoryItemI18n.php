<?php

class CategoryItemI18n extends EActiveRecord 
{
    public $id;
    public $lang_id;
    public $parent_id;
    public $name;
    
    public $dirtyAttributes = array('name');

    public static function model($className = __CLASS__) 
    {
        return parent::model($className);
    }

    public function tableName() 
    {
        return 'category_item_i18ns';
    }

    public function rules() 
    {
        return array(
            array('name', 'filter', 'filter' => 'trim'),
            array('name, lang_id', 'required'),
            array('name', 'length', 'max' => 255),
        );
    }

    public function attributeLabels() 
    {
        return array(
            'name' => 'Name',
        );
    }
}