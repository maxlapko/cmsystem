<?php

class CategoriesModule extends EWebModule
{
    public $defaultController = 'categories';
    
	public function init()
	{
        parent::init();
		// this method is called when the module is being created
		// you may place code here to customize the module or the application
        
		// import the module-level models and components
		$this->setImport(array(
			'categories.models.*',
		));
	}
    
    public function getListCategory($code, $forMenu = true, $throwException = true)
    {
        $category = Category::getByCode($code, $throwException);
        if ($category === null) {
            if ($throwException) {
                throw new CException('The list of categories "' . $code . '" was not found.');
            }
            return array();
        }
        return $category->getAll(null, $forMenu);
    }
    
    public function getListData($code, $throwException = true, $prefix = '&nbsp;&nbsp;')
    {
        $category = Category::getByCode($code);
        if ($category === null) {
            if ($throwException) {
                throw new CException('The list of categories "' . $code . '" was not found.');
            } 
            return array();
        }
        $list = $category->listData(null, 'code', $prefix);
        array_shift($list);
        return $list;
    }
    
    /**
     * @param string $code
     * @return CategoryItem;
     */
    public function getItemByCode($code, $throwException = true)
    {
        $category = CategoryItem::getByCode($code);
        if ($category === null && $throwException) {
            throw new CHttpException(404, 'Unable to find the requested object.');
        }
        return $category;
    }
    
    /**
     * @param string $code
     * @return Category;
     */
    public function getByCode($code, $throwException = true)
    {
        $category = Category::getByCode($code);
        if ($category === null && $throwException) {
            throw new CHttpException(404, 'Unable to find the requested object.');
        }
        return $category;
    }
    
    
    /**
     * @param CActiveRecord $model
     * @return CategoryItem[]
     */
    public function getCategories($model, $parentCode = null)
    {
        if ($model->getIsNewRecord()) {
            return array();
        }
        return CategoryItem::getByEntity($model, $parentCode);
    }
    
    /**
     * @param CActiveRecord $model
     */
    public function setCategories($model, $ids, $parentCode = null)
    {
        return CategoryItem::setForEntity($model, $ids, $parentCode);
    }
    
    /**
     * @param CActiveRecord $model
     */
    public function removeCategories($model, $parentCode = null)
    {
        return CategoryItem::removeByEntity($model, $parentCode);
    }
    
}
