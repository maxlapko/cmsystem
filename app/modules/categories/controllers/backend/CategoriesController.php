<?php

class CategoriesController extends BackendController
{
    public $modelName = 'Category';
    public $redirectTo = array('admin');
    public $defaultAction = 'admin';

    public function actionCreate()
    {
        $this->params['code'] = isset($_GET['code']) ? $_GET['code'] : '';
        unset($_GET['code']);
        parent::actionCreate();
    }
}
