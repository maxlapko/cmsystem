<?php
$this->pageTitle = 'Create content block';
$this->breadcrumbs=array(
	'Manage content blocks' => array('admin'),
	'Create content block',
);

$this->renderPartial('_form', array('model' => $model)); 
?>