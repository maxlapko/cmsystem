<?php /** @var TbActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $model->getFormId(),
        'enableClientValidation' => true,
        'type' => 'horizontal',
    ));
?>
<fieldset>    
    <?php $this->renderPartial('_i18ns', array('model' => $model, 'form' => $form)) ?>    
    <?= $form->textFieldRow($model, 'code', array('class' => 'span8')); ?>        
    <?= $form->textAreaRow($model, 'description', array('class' => 'span8', 'rows' => 5)); ?>
    <?= $form->dropDownListRow($model, 'type', $model::types()); ?>
    
</fieldset>
<?= HApp::saveButtons($model) ?>

<?php $this->endWidget(); ?>
