<?php

class ContentsController extends BackendController
{
    public $modelName = 'ContentBlock';
    public $redirectTo = array('admin');
    public $defaultAction = 'admin';
}