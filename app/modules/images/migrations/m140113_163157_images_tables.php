<?php

class m140113_163157_images_tables extends CDbMigration
{

    public function safeUp()
    {
        $sql = <<< EOD
DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) DEFAULT NULL,  
  `model_type` varchar(32) DEFAULT NULL,  
  `image` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model_type_model_id_idx` (`model_type`, `model_id`),
  KEY `order_idx` (`order`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
EOD;
        $this->execute($sql);
    }

    public function down()
    {
        
    }
}