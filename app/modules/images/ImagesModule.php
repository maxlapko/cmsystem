<?php

class ImagesModule extends EWebModule 
{
    public $defaultController = 'images';
    
    public function init()
    {
        parent::init();
        $this->setImport(array('images.models.*'));
    }
    
    /**
     * @param CActiveRecord $model
     * @param mixed $criteria
     * @param array $params
     * @return Image[]
     */
    public function getImages($model, $criteria = '', $params = array())
    {
        return Image::model()->ordered()->findAllByAttributes(
            array('model_type' => get_class($model), 'model_id' => $model->id),
            $criteria, 
            $params
        );
    }
    
    public function getMediaCount($type, $id)
    {
        return Image::getMediaCount($type, $id);
    }
}
