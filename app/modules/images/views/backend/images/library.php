<?php $this->renderPartial('_form_upload', array('type' => $type, 'id' => $id, 'target' => '#images')); ?>
<div class="row-fluid">
    <div class="span8">
        <ul id="images" class="clearfix" data-sort="<?= $this->createUrl('/images/images/sort')?>">
        <?php foreach ($images as $image) { ?>
            <?= $this->renderPartial('_form', array('model' => $image)); ?>
        <?php } ?> 
        </ul>
    </div>
    <div class="span4">
        <form id="media-form">
            <div class="media-body">
                <?= CHtml::textField('title', '', array('class' => 'span12', 'id' => 'title', 'placeholder' => 'Title')); ?>
                <?= CHtml::textArea('description', '', array('class' => 'span12', 'id' => 'description', 'placeholder' => 'Description')); ?>
                <?= CHtml::textField('url', '', array('class' => 'span12', 'id' => 'url', 'placeholder' => 'Url')); ?>    
                <div>
                    <?= TbHtml::submitButton('Save', array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'class' => 'disabled')); ?>            
                </div>
            </div>
        </form>
    </div>
</div>
