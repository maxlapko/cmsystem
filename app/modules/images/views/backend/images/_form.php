<li class="image-item image-<?= $model->id ?>" data-id="<?= $model->id ?>" id="ids-<?= $model->id ?>">
    <a class="close" data-op="ajax" class="image-delete" href="<?= $this->createUrl('/images/images/delete', array('id' => $model->id)) ?>" data-confirm="Are you ready to delete?">&times;</a>
    <?= CHtml::image($model->getImageUrl('image', 'image_media_preview'), '') ?>
    
    
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => "image-{$model->id}-form",
    'htmlOptions' => array('class' => 'image-item-form pull-left'),
    'action' => array('/images/images/update', 'id' => $model->id)
)); ?>
    <?= $form->hiddenField($model, 'title', array('class' => 'span3', 'id' => "title-{$model->id}")); ?>
    <?= $form->hiddenField($model, 'description', array('class' => 'span3', 'rows' => 3, 'id' => "description-{$model->id}")); ?>
    <?= $form->hiddenField($model, 'url', array('class' => 'span3', 'id' => "url-{$model->id}")); ?>    
<?php $this->endWidget(); ?>
</li>

