<?php
$this->pageTitle= 'Manage Pages';

$this->breadcrumbs=array(
	'Pages',
);
?>

<h1>Manage Pages</h1>

<?= TbHtml::link('Create new page', array('create'), array('class' => 'btn')); ?>
<br />
<br />

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id'           => $model->getGridId(),
    'dataProvider' => $model->search(),
    'filter'       => $model,
    'template'     => "{items}\n{pager}",
    'columns'      => array(
        array(
            'header'   => 'Title',
            'type'     => 'raw',
            'value'    => 'CHtml::link(CHtml::encode($data->getI18n("title")), $data->getUrl())',
            'sortable' => false,
        ),
        array(
            'name' => 'code'
        ),
        array(
            'name'   => 'status',
            'value'  => 'Page::statuses($data->status)',
            'filter' => Page::statuses()
        ),
        array(
            'name'     => 'created_at',
            'filter'   => false,
            'sortable' => false,
        ),
        array(
            'class'              => 'bootstrap.widgets.TbButtonColumn',
            'deleteConfirmation' => 'Are you sure to delete this page?',
            'viewButtonUrl'      => '$data->url',
            'htmlOptions'        => array('class' => 'buttons_column'),
        ),
    ),
)); 
?>