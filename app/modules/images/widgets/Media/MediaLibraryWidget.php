<?php

class MediaLibraryWidget extends CWidget
{
    public $model;
    
    public function run()
    {
        $this->_publish();
        $class = get_class($this->model);
        $id = $this->model->id;        
        echo CHtml::link(
            'Media Library (<span class="media-lib-counter">' . Yii::app()->getModule('images')->getMediaCount($class, $id) . '</span>)' , 
            array('/images/images/library', 'type' => $class, 'id' => $id), 
            array(
                'class' => 'btn btn-success', 
                'data-op' => 'modal', 
                'data-title' => 'Upload images', 
                'data-skip' => 1,
                'data-class' => 'big',
            )
        );
    }
    
    protected function _publish()
    {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->getAssetManager()->publish($assets, false, -1, YII_DEBUG);
        if (is_dir($assets)) {
            Yii::app()->getClientScript()
                ->registerScriptFile($baseUrl . '/media.js', CClientScript::POS_END);
        } else {
            throw new CHttpException(500, 'Error: Could not find assets to publish.');
        }
    }
}

