<?php

class ImagesController extends BackendController
{
    public $modelName = 'Image';
    public $redirectTo = array('admin');
    public $defaultAction = 'admin';
    
    public function actionUpload($type, $id)
    {
        $image = new Image;
        $image->model_type = $type;
        $image->model_id = $id;
        
        if ($image->validate()) {
            $image->uploadImage(CUploadedFile::getInstance($image, 'image'), 'image');            
            if ($image->save(false)) {
                $this->renderJson(array(
                    'success' => true, 
                    'id' => $image->id, 
                    'html' => $this->renderPartial('_form', array('model' => $image), true)
                ));               
            }
        } else {
            $this->renderJson(array('success' => false, 'message' => $image->getError('image')));
        }
    }
    
    public function actionUpdate($id)
    {
        $model = $this->loadModel($this->modelName, $id);
        
        if (isset($_POST[$this->modelName])) {
            $model->setAttributes($_POST[$this->modelName]);
            $this->renderJson(array('success' => $model->save()));
        }
        $this->renderJson(array('success' => false));
    }
    
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $this->loadModel($this->modelName, $id)->delete();
            $this->renderJson(array('success' => true, 'html' => '', 'target' => ".image-$id"));
        } else {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }
    
    public function actionRedactorUpload()
    {
        $image = new Image;
        $image->model_type = 'Redactor';
        $image->model_id = 1;
        $image->image = CUploadedFile::getInstanceByName('file');
        if ($image->validate()) {
            $image->uploadImage($image->image, 'image');            
            if ($image->save(false)) {
                $this->renderJson(array('success' => true, 'filelink' => $image->getImageUrl('image', 'orig')));
            }
        } else {
            $this->renderJson(array('success' => false, 'message' => $image->getError('image')));
        }
    }
    
    public function actionRedactorIndex()
    {
        $result = array();
        foreach (Image::model()->findAll() as $image) {
            $result[] = array(
                'thumb' => $image->getImageUrl('image', 'image_preview'),
                'image' => $image->getImageUrl('image', 'orig'),
            );
        }
        $this->renderJson($result);
    }
    
    public function actionLibrary($type, $id)
    {
        $images = Image::model()->ordered()->findAllByAttributes(array('model_type' => $type, 'model_id' => $id));
        $this->render('library', compact('type', 'id', 'images'));
    }
    
    public function actionSort()
    {
        if (!empty($_POST['ids'])) {
            Image::sort($_POST['ids']);
        }        
        echo CJSON::encode(array('success' => true));
    }
    
}