<?php

class Image extends EActiveRecord
{
    public $id;
    public $image;
    public $order;
    public $title;
    public $description;
    public $url;
    public $model_type;
    public $model_id;
    
    public $created_at;
    public $updated_at;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'images';
    }

    public function rules()
    {
        return array(
            array('model_type, model_id', 'required'),
            array('url, title, description', 'safe'),
            array(
                'image', 'ext.components.image_processor.MImageValidator', 
                'types' => array('jpg', 'png', 'jpeg'), 'maxSize' => 5 * 1024 * 1024, 'on' => array('insert')
            ),
            array('model_type, model_id', 'safe', 'on' => 'search'),
        );
    }
    
    public function behaviors ()
    {
        return array(
            'CTimestampBehavior' => array(
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'createAttribute'   => 'created_at',
                'updateAttribute'   => 'updated_at',
                'setUpdateOnCreate' => true
            ),
            'mImage' => array(
                'class'          => 'ext.components.image_processor.MImageBehavior',
                'imageProcessor' => 'image' // image processor component name 
            )
        );
    }

    public function attributeLabels()
    {
        return array(
            'id'               => 'ID',
            'title'            => 'Title',
            'image'            => 'Image',
            'url'              => 'Url',
            'description'      => 'Description',
            'model_type'       => 'Model type',
            'model_id'         => 'Model ID',
            'created_at'       => 'Create date',
            'updated_at'       => 'Update date',
            'order'            => 'Order',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('model_type', $this->model_type);
        $criteria->compare('model_id', $this->model_id);

        return new CActiveDataProvider($this, array(
            'criteria'   => $criteria,
            'sort'       => array(
                'defaultOrder' => '`order` ASC',
            ),
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));
    }
    
    public function scopes()
    {
        return array(
            'ordered' => array(
                'order' => '`order` ASC'
            )
        );
    }
    
    public static function getMediaCount($type, $id)
    {
        return Yii::app()->getDb()->createCommand(
            'SELECT COUNT(*) FROM images WHERE `model_type` = :type AND `model_id` = :id'
        )->queryScalar(array(
            ':type' => $type, ':id' => $id
        ));
    }
    
    public static function sort($ids) 
    {
        return parent::sorting('images', $ids);
    }
    
    protected function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            $this->order = $this->getMaxOrder('`model_type` = :type', array(
                ':type' => $this->model_type
            )) + 1;
        }
        return parent::beforeSave();
    }
    
    protected function afterDelete()
    {
        parent::afterDelete();
        $class = $this->model_type;
        $presents = array('image_preview', 'orig');
        if (@class_exists($class)) {
            $presents = array_merge($presents, $class::getImagePresets());            
        }
        $this->deleteImage('image', $presents);
    }
    
}