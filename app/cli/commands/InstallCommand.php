<?php

class InstallCommand extends CConsoleCommand
{        
    public function actionIndex()
    {
        $config = require(Yii::getPathOfAlias('application.config') . '/main.php');
        $modules = isset($config['modules']) ? $config['modules'] : array();
        foreach ($modules as $key => $value) {
            $name = is_integer($key) ? $value : $key;
            $alias = 'application.modules.' . $name . '.migrations';
            if (is_dir(Yii::getPathOfAlias($alias))) {
                $this->getCommandRunner()->run(array('yiic', 'migrate', '--migrationPath=' . $alias, '--interactive=0'));
            }
        }
        $this->getCommandRunner()->run(array('yiic', 'migrate', '--interactive=0'));
    }
}