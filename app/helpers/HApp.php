<?php

class HApp
{
    public static function isFront()
    {
        static $isFront;
        if ($isFront === null) {
            $isFront = Yii::app()->getComponent('admin') !== null;
        }
        return $isFront;
    }
    
    /**
     * @param CActiveRecord $model
     */
    public static function attachMetaParams($model)
    {
        $controller = Yii::app()->getController();
        if ($model instanceof I18nActiveRecord) {
            if ($title = $model->getI18n('meta_title')) {
                $controller->setPageTitle($title);
            } else {
                $controller->setPageTitle($model->getI18n('title'));
            }
            $controller->metaKeywords = $model->getI18n('meta_keywords');
            $controller->metaDescription = $model->getI18n('meta_description');
        } else {
            if ($model->hasAttribute('meta_title')) {
                $controller->setPageTitle($model->getAttribute('meta_title'));
            }
            if ($model->hasAttribute('meta_keywords')) {
                $controller->metaKeywords = $model->getAttribute('meta_keywords');
            }
            if ($model->hasAttribute('meta_description')) {
                $controller->metaDescription = $model->getAttribute('meta_description');
            }            
        }
    }
    
    public static function createMenuUrl($url)
    {
        if (stripos($url, 'http') === 0 || strpos($url, '/') !== 0) {
            return $url;
        }
        $default = Yii::app()->getModule('languages')->getDefault();
        $lang = Yii::app()->getLanguage();
        if ($default != $lang) {
            $url = '/' . $lang . $url;
        }
        return $url;
    }
    
    public static function detectMenuActive($url, $condition = null)
    {
        $currentUrl = Yii::app()->getRequest()->getRequestUri();
        return $url === '/' ? $currentUrl === '/' : stripos($currentUrl, $url) === 0;
    }
    
    public static function saveButtons($model)
    {
        $buttons = array(
            TbHtml::submitButton($model->getIsNewRecord() ? 'Create' : 'Save', array('color' => TbHtml::BUTTON_COLOR_PRIMARY)),
            TbHtml::submitButton($model->getIsNewRecord() ? 'Create and continue' : 'Save and continue', array('name' => 'continue', 'color' => TbHtml::BUTTON_COLOR_SUCCESS))
        );
        if (!empty($_REQUEST['redirect'])) {
            $buttons[] = TbHtml::submitButton($model->getIsNewRecord() ? 'Create and back' : 'Save and back', array('name' => 'back', 'color' => TbHtml::BUTTON_COLOR_INFO));
        }
        return TbHtml::formActions($buttons);
    }
    
    public static function editLink($model, $params = array())
    {
        return CHtml::link(
            '', 
            $model->getEditUrl(array_merge($params, array('redirect' => Yii::app()->getRequest()->getRequestUri()))), 
            array('class' => 'icon-pencil')
        );
    }
    
    public static function printOption($data, $delim = ', ')
    {
        return implode($delim, array_map(function($model) {
            return $model->getI18n('name');
        }, $data));
    }
}