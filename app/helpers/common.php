<?php

/**
 * @link CHtml::link
 */
function h($text)
{
    return CHtml::encode($text);
}

function l($text, $url = '#', $htmlOptions = array())
{
    return CHtml::link($text, $url, $htmlOptions);
}

function param($name, $default = null)
{
    $params = Yii::app()->getParams();
    return $params->contains($name, $default) ? $params->itemAt($name) : $default;
}

function t($category, $message, $params = array(), $source = null, $language = null)
{
    return Yii::t($category, $message, $params, $source, $language);
}

function block($code, $data = array(), $lang = null)
{
    return Yii::app()->getModule('contentblock')->render($code, $data, $lang);
}

function rrmdir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        if ($objects) {
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . DIRECTORY_SEPARATOR . $object) == "dir")
                        rrmdir($dir . DIRECTORY_SEPARATOR . $object);
                    else
                        unlink($dir . DIRECTORY_SEPARATOR . $object);
                }
            }
        }
        reset($objects);
        rmdir($dir);
    }
}

function toBytes($str)
{
    $val = trim($str);
    $last = strtolower($str[strlen($str) - 1]);
    switch ($last) {
        case 'g': $val *= 1024;
        case 'm': $val *= 1024;
        case 'k': $val *= 1024;
    }
    return $val;
}
