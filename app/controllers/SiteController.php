<?php
/**
 *
 * SiteController class
 *
 */
class SiteController extends EController
{
    
    public function actionIndex()
    {
        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionLogin()
    {
        if (!Yii::app()->getUser()->getIsGuest()) {
            $this->redirect(array('site/index'));
        }
        $model = new LoginForm;
        if (isset($_POST['LoginForm'])) {
            $model->setAttributes($_POST['LoginForm']);
            if ($model->validate() && $model->login()) {
                $this->redirect(Yii::app()->getUser()->getReturnUrl());
            }
        }
        $this->render('login', array('model' => $model));
    }

}