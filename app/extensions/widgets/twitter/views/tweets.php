<div class="last-twits" id="<?= $this->getId() ?>">
    <div class="scrolling">
        <div class="title"><?= Yii::t('site', 'Latest Tweets') ?></div>
        <?php foreach ($tweets as $tweet) : ?>
        <div class="item">
            <time><?= Yii::app()->getLocale()->getDateFormatter()->formatDateTime(strtotime($tweet['created_at']), 'medium', 'short'); ?></time>
            <p><?= $this->processText($tweet['text']) ?></p>
        </div>
        <?php endforeach; ?>
    </div>
</div>