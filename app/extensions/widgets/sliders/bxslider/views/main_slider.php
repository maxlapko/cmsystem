<div id="main-slider-container">    
    <ul id="<?= $this->getId() ?>">
        <?php foreach ($this->items as $image) : ?>
        <li>
            <img src="<?= $image->getImageUrl('image', $this->imagePreset) ?>" alt="" />
            <div class="caption">
                <div class="title"><?= h($image->title) ?></div>
                <p><?= h($image->description) ?></p>
            </div>
        </li>
        <?php endforeach ?>
    </ul>
</div>