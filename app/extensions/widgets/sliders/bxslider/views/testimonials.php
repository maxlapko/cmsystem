<ul id="<?= $this->getId() ?>">
    <?php foreach ($this->items as $page) : ?>
    <li>
        <div class="item">
            <div class="title"><?= $page->getI18n('title')?></div>
            <span><?= strip_tags($page->getI18n('short_content')) ?></span>
            <p><?= strip_tags($page->getI18n('content')) ?></p>
        </div>
    </li>
    <?php endforeach ?>
</ul>