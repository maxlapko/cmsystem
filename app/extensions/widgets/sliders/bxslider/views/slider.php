<ul id="<?= $this->getId() ?>">
    <?php foreach ($this->items as $image) : ?>
    <li>
        <div class="media">
            <div class="capture">
                <?= HUi::prepareImageDescription($image->description) ?>
            </div>
            <?php if ($image->url) : ?>
                <?= HUi::videoLink($image->url, 978, 550)?>
            <?php else : ?>
                <img src="<?= $image->getImageUrl('image', $this->imagePreset) ?>" alt=""/>
            <?php endif;?>
        </div>
    </li>
    <?php endforeach ?>
</ul>