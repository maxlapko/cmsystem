<?php

/**
 * Description of SelectorWidget
 *
 * @author mlapko
 */
class BxsliderWidget extends CWidget
{
    public $code;
    
    public $options = array();
    
    public $view = 'slider';
    
    public $items;
    
    public $imagePreset = 'orig';


    public function init()
    {
        parent::init();
        $this->_publish();
    }

    protected function _publish()
    {   
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->getAssetManager()->publish($assets, false, -1, YII_DEBUG);
        if (is_dir($assets)) {
            $cs = Yii::app()->getClientScript()
                ->registerCoreScript('jquery')
                ->registerScriptFile($baseUrl . '/bxslider/jquery.bxslider.js', CClientScript::POS_END)
                ->registerCssFile($baseUrl . '/bxslider/jquery.bxslider.css');
            $cs->registerScript('slider' . $this->getId(), '
                $("#' . $this->getId() . '").bxSlider(' . CJavaScript::encode($this->options) . ');
            ');
        } else {
            throw new CHttpException(500, get_class($this) . ' - Error: Could not find assets to publish.');
        }
    }
    
    public function run()
    {
        if ($this->items === null) {
            $gallery = Yii::app()->getModule('gallery')->getByCode($this->code);
            if ($gallery === null) {
                return;
            }
            $this->items = $gallery->getImages();
        }
        $this->render($this->view);
    }
}