;(function($) {
var COOKIE_NAME = 'admin-toolbar',
    Toolbar = {
        init: function() {
            this.$switcher = $('#a-toolbar-switcher');
            this.$toolbar = $('#a-toolbar');
            
            if ($.cookie(COOKIE_NAME)) {
                this.$toolbar.hide();
            } else {
                this.$toolbar.show();
            }
            
            this.$switcher.bind('click', $.proxy(this.toggle, this));
        },
        toggle: function(e) {
            e.preventDefault();
            if (this.$toolbar.is(":visible")) {
                this.$toolbar.hide();
                $.cookie(COOKIE_NAME, 'hide', {path: '/', expires: 10});
            } else {
                this.$toolbar.show();
                $.cookie(COOKIE_NAME, null, {path: '/', expires: -1});
            }
        }
    }
    
$(function() {
    Toolbar.init();
});
    
})(jQuery);

