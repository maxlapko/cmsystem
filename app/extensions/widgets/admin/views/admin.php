<div id="a-toolbar-switcher"></div>
<div id="a-toolbar">
    <div class="title"><a target="_blank" href="/backend.php"><?= Yii::app()->name . ' admin' ?></a></div>
    <div class="menu">
        <?php 
            $controller = $this->getController();
            if (property_exists($controller, 'aMenu')) {
                $controller->widget('zii.widgets.CMenu', array(
                    'items' => $controller->aMenu
                ));
            }
        ?>
    </div>
</div>
