<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminWidget
 *
 * @author mlapko
 */
class AdminWidget extends CWidget
{
    public $component = 'admin';
    
    public function run()
    {
        $admin = $this->getComponent();
        if (!$admin->getIsGuest()) {
            $this->_publish();
            $this->render('admin');
        }
    }
    

    /**
     * @return WebUser
     */
    public function getComponent()
    {
        static $component;
        if ($component === null) {
            $component = Yii::app()->getComponent($this->component);
        }
        return $component;
    }
    
    protected function _publish()
    {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->getAssetManager()->publish($assets, false, -1, YII_DEBUG);
        if (is_dir($assets)) {
            Yii::app()->getClientScript()->registerCssFile($baseUrl . '/admin.css')
                ->registerCoreScript('jquery')
                ->registerCoreScript('cookie')
                ->registerScriptFile($baseUrl . '/admin.js', CClientScript::POS_END);
        } else {
            throw new CHttpException(500, 'FUploader - Error: Could not find assets to publish.');
        }
    }
    
}
