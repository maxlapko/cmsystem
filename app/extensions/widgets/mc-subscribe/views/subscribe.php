<div class="newsletter-box">
    <h3><?= Yii::t('site', 'Newsletter Sign Up') ?></h3>
    
    <?php 
        $user = Yii::app()->getUser();
        if ($user->hasFlash('SubscribeWidget')) { ?>
            <div class="subscribe-form-success"><?= $user->getFlash('SubscribeWidget') ?></div>
            <?php Yii::app()->getClientScript()->registerScript('SubscribeWidget', '$("html,body").delay(500).animate({ scrollTop : $(window).height() }, "slow");');

        } else { 
            $form = $this->beginWidget('CActiveForm', array(
                'id'     => 'subscribe-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
                'htmlOptions' => array(
                    'enctype' => 'multipart/form-data'
                )
            )); ?>
            <div class="send-input">
                <?= $form->textField($model, 'email', array('placeholder' => Yii::t('site', 'Enter your email..'))); ?>
                <button type="submit"></button>
            </div>
            <?= $form->error($model, 'email'); ?>
            <?php $this->endWidget(); ?>
    <?php } ?>

</div>
