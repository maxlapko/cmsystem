<?php

class FUploader extends CComponent
{

    public static function publishAssets()
    {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->getAssetManager()->publish($assets);
        if (is_dir($assets)) {
            Yii::app()->getClientScript()->registerScriptFile($baseUrl . '/fileuploader.js', CClientScript::POS_END)
                ->registerCssFile($baseUrl . '/fileuploader.css');
        } else {
            throw new CHttpException(500, 'FUploader - Error: Could not find assets to publish.');
        }
    }

    public static function add($selector = 'fileupload', $options = array())
    {
        self::publishAssets();
        $options['element'] = 'js:document.getElementById("' . $selector . '")';
        Yii::app()->getClientScript()->registerScript(
            __CLASS__ . uniqid(), 
            'var uploader = new qq.FileUploader(' . CJavaScript::encode($options) . ');', 
            CClientScript::POS_READY
        );
    }
}