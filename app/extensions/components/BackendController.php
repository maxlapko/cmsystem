<?php


class BackendController extends EController
{

    public $layout = '//layouts/main';
    
    public $params = array();
    public $photoUpload = false;
    public $scenario = null;
    public $with = array();
    public $redirectTo;
    public $modelName;
    
    protected $_model = null;

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'users'   => array('@'),
//                'roles'   => array(Admin::ROLE_ADMIN)
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id)
    {
        $this->render('view', array('model' => $this->loadModel($this->modelName, $id)));
    }

    public function actionCreate()
    {
        /** @var CActiveRecord $model */
        $model = new $this->modelName;
        if ($this->scenario) {
            $model->scenario = $this->scenario;
        }
        $this->performAjaxValidation($model);

        if (isset($_POST[$this->modelName])) {
            $model->setAttributes($_POST[$this->modelName]);
            if ($model->validate()) {
                if ($model->hasAttribute('image') && $model->asa('mImage')) {
                    $model->uploadImage(CUploadedFile::getInstance($model, 'image'), 'image');
                }
                $transaction = $model->getDbConnection()->beginTransaction();
                try {
                    $model->save(false);
                    $transaction->commit();
                    Yii::app()->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('common', 'Successfully created.'));
                    $this->_redirect($model);
                } catch (Exception $err) {
                    $transaction->rollback();
                    Yii::app()->getUser()->setFlash(
                        TbHtml::ALERT_COLOR_ERROR, 
                        Yii::t('common', 'System error: ' . $err->getMessage() . '.')
                    );
                }
            }
        }

        $this->render('create', array_merge(
            array('model' => $model), $this->params
        ));
    }

    public function actionUpdate($id)
    {
        if ($this->_model === null) {
            $model = $this->loadModel($this->modelName, $id);
        } else {
            $model = $this->_model;
        }

        $this->performAjaxValidation($model);

        if (isset($_POST[$this->modelName])) {
            $model->setAttributes($_POST[$this->modelName]);
            if ($model->validate()) {
                if ($model->hasAttribute('image') && $model->asa('mImage')) {
                    $this->photoUpload = $model->uploadImage(CUploadedFile::getInstance($model, 'image'), 'image');
                }
                $transaction = $model->getDbConnection()->beginTransaction();
                try {
                    $model->save(false);
                    $transaction->commit();
                    Yii::app()->getUser()->setFlash(TbHtml::ALERT_COLOR_SUCCESS, Yii::t('common', 'Successfully updated.'));
                    $this->_redirect($model);
                } catch (Exception $err) {
                    $transaction->rollback();
                    Yii::app()->getUser()->setFlash(
                        TbHtml::ALERT_COLOR_ERROR, 
                        Yii::t('common', 'System error: ' . $err->getMessage() . '.')
                    );
                }
            }
        }

        $this->render('update', array_merge(array('model' => $model), $this->params));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // we only allow deletion via POST request
            $this->loadModel($this->modelName, $id)->delete();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider($this->modelName);
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAdmin()
    {
        $model = new $this->modelName('search');
        if ($this->scenario) {
            $model->scenario = $this->scenario;
        }
        if ($this->with) {
            $model = $model->with($this->with);
        }
        $model->resetScope();

        $model->unsetAttributes();  // clear any default values
        if (isset($_GET[$this->modelName])) {
            $model->setAttributes($_GET[$this->modelName]);
        }
        $this->render('admin', array_merge(array('model' => $model), $this->params));
    }

    public function loadModelWith($with)
    {
        if (isset($_GET['id'])) {
            $model = new $this->modelName;
            if ($this->scenario) {
                $model->scenario = $this->scenario;
            }
            if ($model === null) {
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            return $model->resetScope()->with($with)->findByPk($_GET['id']);
        }
    }

    public function actionMove($id, $dir)
    {
        $model = $this->loadModel($this->modelName, $id);

        if ($dir == 'up' || $dir == 'down') {
            return $model->moveEntry($dir);
        } 
        return $model;
    }

    public function getMinSorter()
    {
        $model = new $this->modelName;
        $minSorter = Yii::app()->db->createCommand()
            ->select('MIN(sorter) as maxSorter')
            ->from($model->tableName())
            ->queryScalar();
        $this->params['minSorter'] = $minSorter;
        return $minSorter;
    }

    public function actionActivate()
    {
        $field = isset($_GET['field']) ? $_GET['field'] : 'active';

        if (isset($_GET['id']) && isset($_GET['action'])) {
            $action = $_GET['action'];
            $model = $this->loadModel($this->modelName, $_GET['id']);
            if ($this->scenario) {
                $model->scenario = $this->scenario;
            }
            if ($model) {
                $model->$field = ($action == 'activate' ? 1 : 0);
                $model->update(array($field));
            }
        }
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    public function actionItemsSelected()
    {
        $idsSelected = Yii::app()->getRequest()->getPost('itemsSelected');

        $work = Yii::app()->getRequest()->getPost('workWithItemsSelected');

        if ($idsSelected && is_array($idsSelected) && $work) {
            $idsSelected = array_map('intval', $idsSelected);

            foreach ($idsSelected as $id) {
                $model = $this->loadModel($id);
                $model->scenario = 'changeStatus';

                if ($work == 'delete') {
                    $model->delete();
                } elseif ($work == 'activate') {
                    $model->active = 1;
                    $model->update('active');
                } elseif ($work == 'deactivate') {
                    $model->active = 0;
                    $model->update('active');
                }
            }
        }

        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    
    protected function _redirect($model)
    {
        if (isset($_POST['continue'])) {
            $this->redirect(array('update', 'id' => $model->id));
        } elseif (isset($_POST['back']) && !empty($_REQUEST['redirect'])) {
            $this->redirect($_REQUEST['redirect']);
        } elseif ($this->redirectTo) {
            $this->redirect($this->redirectTo);
        } else {
            $this->redirect(array('view', 'id' => $model->id));
        }
    }

}
