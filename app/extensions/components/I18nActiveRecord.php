<?php

/**
 * I18nActiveRecord class
 *
 * Some cool methods to share amount your models
 */
class I18nActiveRecord extends EActiveRecord
{    
    public $i18nRelation = 'i18ns';
    public $i18nModel;    
        
    public function withLang($lang = null)
    {
        if ($this->i18nRelation && isset($this->getMetaData()->relations[$this->i18nRelation])) {
            $criteria = $this->getDbCriteria();
            $criteria->with[$this->i18nRelation] = $this->getI18nScope($lang);            
        }
        return $this;
    }
    
    /**
     * @param $language
     * @return array 
     */
    public function getI18nScope($language = null)
    {
        if ($language === null) {
            $language = Yii::app()->getLanguage();
        }
        $langs = array($language, Yii::app()->getModule('languages')->getDefault());
        return array(
            'on'    => $this->i18nRelation . '.lang_id IN ("' . implode('", "', array_unique($langs)) . '")',
            'index' => 'lang_id'
        );        
    }
    
    public function setI18n($data, $language = null, $forceSave = false)
    {
        if ($language === null) {
            $language = Yii::app()->getModule('languages')->getDefault();
        }
        $i18ns = $this->getRelated($this->i18nRelation);
        $i18nModel = isset($i18ns[$language]) ? $i18ns[$language] : new $this->i18nModel;
        foreach ($data as $key => $value) {
            $i18nModel->$key = $value;
        }
        $i18nModel->lang_id = $language;
        
        $i18ns[$language] = $i18nModel;
        if ($forceSave) {
            $i18nModel->save(false);
        }        
        $this->{$this->i18nRelation} = $i18ns;
    }

    /**
     * Return content 
     * @param string $attribute
     * @param boolean $info
     * @param string $language
     * @return mixed if info = true return array(text, incorrect flag, model)
     */
    public function getI18n($attribute, $info = false, $language = null)
    {
        $language = $language === null ? Yii::app()->getLanguage() : $language;
        if ($this->hasRelated($this->i18nRelation)) {
            $relations = $this->getRelated($this->i18nRelation);            
        } else {
            $relations = $this->getRelated($this->i18nRelation, false, $this->getI18nScope($language));
        }
        if (count($relations) === 0) {
            return $info ? array(null, true, null) : null;
        } elseif (isset($relations[$language]) && $relations[$language]->$attribute !== '') { // exists current language
            return $info ? array($relations[$language]->$attribute, false, $relations[$language]) : $relations[$language]->$attribute;
        } elseif (isset($relations[$defaultLang = Yii::app()->getModule('languages')->getDefault()]) && $relations[$defaultLang]->$attribute !== '') { // exists default language           
            return $info ? array($relations[$defaultLang]->$attribute, true, $relations[$defaultLang]) : $relations[$defaultLang]->$attribute;
        }
        return $info ? array(null, true, null) : null;
    }
    
    /**
     * Clear dirty attributes for default language
     * @param array $relations
     */
    public function clearDirtyAttributes($relations) 
    {
        $default = Yii::app()->getModule('languages')->getDefault();
        if (isset($relations[$default])) {
            $relations[$default]->dirtyAttributes = null;
        }
    }
    
    protected function afterDelete()
    {
        parent::afterDelete();
        $class = $this->i18nModel;
        $class::model()->deleteAllByAttributes(array('parent_id' => $this->id));
    }
}
