<?php

/**
 * EWebModule class
 */
class EWebModule extends CWebModule
{
    protected function init()
    {
        $app = Yii::app();
        if ($app->asa('runEnd')) {
            $endName = $app->getEndName();
            $this->setControllerPath($this->getControllerPath() . DIRECTORY_SEPARATOR . $endName);
            if ($endName === 'backend') {
                $this->setViewPath($this->getViewPath() . DIRECTORY_SEPARATOR . $endName);
            }
        }
    }
    
    public function getStringEntity()
    {
        return '';
    }
    
}
