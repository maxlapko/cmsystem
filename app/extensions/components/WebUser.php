<?php

class WebUser extends CWebUser
{
    private $_model = null;
    
    public $isAdmin = false;
    
    /**
     * Get user model
     * @return User 
     */
    public function getModel()
    {
        if (!$this->getIsGuest() && $this->_model === null) {
            if ($this->isAdmin) {
                $this->_model = Admin::model()->findByPk($this->getId());
            } else {
                $this->_model = User::getById($this->getId());
            }
        }
        return $this->_model;
    }
    
    public function getRole()
    {
        $model = $this->getModel();
        if ($model === null) {
            return 'guest';
        } 
        Yii::app()->getComponent('authManager')->assign($model->role, $model->id);
        return $model->role;
    }
}