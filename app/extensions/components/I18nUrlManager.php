<?php

/**
 * Description of I18nUrlManager
 *
 * @author mlapko
 */
class I18nUrlManager extends CUrlManager
{
    public $languageParam = 'language';
    public $languageModule  = 'languages';
    
    public function init()
    {
        $langs = Yii::app()->getModule($this->languageModule)->listing();
        
        if (count($langs) < 2) {
            return parent::init();
        }
        
        $strLang = implode('|', array_keys($langs));
        
        $newRules = array();
        foreach ($this->rules as $rule => $p) {
            $rule = ($rule[0] == '/'
                ? '/<' . $this->languageParam . ':(' . $strLang. ')>'
                : '<' . $this->languageParam . ':(' . $strLang . ')>/'
            ) . $rule;
            $newRules[$rule] = $p;
        }
        $this->rules = array_merge(
            $newRules, $this->rules
        );
        
        parent::init();
    }

    /**
     * Parses the user request.
     * @param CHttpRequest $request the request application component
     * @return string the route (controllerID/actionID) and perhaps GET parameters in path format.
     */
    public function parseUrl($request)
    {
        $route = parent::parseUrl($request);
        $module = Yii::app()->getModule($this->languageModule);
        $module->setLanguage($module->getLanguage());
        return $route;
    }
    
    public function createUrl($route, $params = array(), $ampersand = '&')
    {
        $defaultLanguage = Yii::app()->getModule($this->languageModule)->getDefault();
        if ($defaultLanguage !== Yii::app()->getLanguage() && !isset($params[$this->languageParam])) {
            $params[$this->languageParam] = Yii::app()->getLanguage();            
        } elseif (isset($params[$this->languageParam]) && $params[$this->languageParam] == $defaultLanguage) {
            unset($params[$this->languageParam]);
        }
        return parent::createUrl($route, $params, $ampersand);
    }
        
}