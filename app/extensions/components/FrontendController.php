<?php


class FrontendController extends EController
{

    public $layout = '//layouts/main';
    
    public $with = array();
    public $modelName;
    
    /**
     * @var admin menu for panel 
     */
    public $aMenu = array();
    
    protected $_model = null;
    

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function actionView($id)
    {
        $this->render('view', array('model' => $this->loadModel($this->modelName, $id)));
    }
    
    public function actionShow($slug)
    {
        $model = $this->loadModelBySlug($this->modelName, $slug);
        HApp::attachMetaParams($model);
        $this->render('view', array('model' => $model));
    }

    public function actionIndex()
    {
        $m = $this->getModule();
        $this->aMenu = array(
            array(
                'label' => 'Manage ' . $m->getStringEntity(), 
                'url' => array('/backend.php/' . $m->getId() . '/' . $m->defaultController. '/admin')
            ),
            array(
                'label' => 'Create ' . $m->getStringEntity(), 
                'url' => array('/backend.php/' . $m->getId() . '/' . $m->defaultController. '/create')
            ),
        );
        $dataProvider = new CActiveDataProvider($this->modelName);
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

}
