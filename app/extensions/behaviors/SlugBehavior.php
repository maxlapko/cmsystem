<?php
/**
 * SlugBehavior class file.
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @license http://www.opensource.org/licenses/bsd-license.php
 */

/**
 * Class SlugBehavior
 *
 * @method CActiveRecord getOwner()
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @version 0.1
 * @package yiiext/slug-behavior
 */
class SlugBehavior extends CBehavior
{
	/** @var string|callable */
	public $sourceAttribute = 'title';
	/** @var string */
	public $slugAttribute = 'slug';
	/** @var bool */
	public $lowercase = true;
	/** @var string */
	public $delimiter = '-';
	/** @var int */
	public $length;
	/** @var array */
	public $replacements = array(
        "А" => "A", "Б" => "B", "В" => "V", "Г" => "G",
        "Д" => "D", "Е" => "E", "Ж" => "J", "З" => "Z", "И" => "I",
        "Й" => "Y", "К" => "K", "Л" => "L", "М" => "M", "Н" => "N",
        "О" => "O", "П" => "P", "Р" => "R", "С" => "S", "Т" => "T",
        "У" => "U", "Ф" => "F", "Х" => "H", "Ц" => "TS", "Ч" => "CH",
        "Ш" => "SH", "Щ" => "SCH", "Ъ" => "", "Ы" => "YI", "Ь" => "",
        "Э" => "E", "Ю" => "YU", "Я" => "YA", "а" => "a", "б" => "b",
        "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ж" => "j",
        "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l",
        "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
        "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h",
        "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "y",
        "ы" => "yi", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
    );
	/** @var callable */
	public $translator;
	/** @var array */
	public $scenarios = array('insert', 'update');

	public function events()
	{
		return array(
			'onBeforeValidate' => 'addSlugValidators',
		);
	}

	public function addSlugValidators()
	{
		$owner = $this->getOwner();

		if (in_array($owner->getScenario(), $this->scenarios)) {
			$list = $owner->getValidatorList();
			$list->add(CValidator::createValidator('validateExistsSlug', $this, $this->slugAttribute));
			$list->add(CValidator::createValidator('validateUniqueSlug', $this, $this->slugAttribute));
		}
	}

	public function validateExistsSlug()
	{
		$owner = $this->getOwner();
        $title = is_callable($this->sourceAttribute) ? 
            call_user_func($this->sourceAttribute, $owner) :
            $owner->getAttribute($this->sourceAttribute);
            
		if (!empty($title)) {
			$owner->setAttribute($this->slugAttribute, $this->generateSlug($title));
		}
	}

	public function validateUniqueSlug()
	{
		$owner = $this->getOwner();
		CValidator::createValidator('unique', $owner, $this->slugAttribute)->validate($owner, $this->slugAttribute);
		if (is_string($this->sourceAttribute)) {
            $owner->addErrors(array(
                $this->sourceAttribute => $owner->getErrors($this->slugAttribute)
            ));
        }
	}

	public function findBySlug($slug, $condition = '', array $params = array())
	{
		return $this->filterBySlug($slug)->find($condition, $params);
	}

	public function filterBySlug($slug, $operator = 'AND')
	{
		$this->getOwner()->getDbCriteria()->compare($this->slugAttribute, $slug, false, $operator);
		return $this->getOwner();
	}

	protected function generateSlug($string)
	{
		// Make sure string is in UTF-8 and strip invalid UTF-8 characters
		$string = mb_convert_encoding((string) $string, 'UTF-8', mb_list_encodings());

		// Make custom replacements
		$string = strtr($string, $this->replacements);

		if (is_callable($this->translator)) {
			$string = call_user_func($this->translator, $string);
		}

		// Replace non-alphanumeric characters with our delimiter
		$string = preg_replace('/[^\p{L}\p{Nd}]+/u', $this->delimiter, $string);

		// Remove duplicate delimiters
		$string = preg_replace('/(' . preg_quote($this->delimiter, '/') . '){2,}/', '$1', $string);

		if ((int) $this->length > 0) {
			$string = mb_substr($string, 0, $this->length, 'UTF-8');
		}

		// Remove delimiter from ends
		$string = trim($string, $this->delimiter);

		return $this->lowercase ? mb_strtolower($string, 'UTF-8') : $string;
	}
}
