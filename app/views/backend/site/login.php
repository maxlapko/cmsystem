<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form TbActiveForm  */

$this->pageTitle = 'Please sign in / ' . Yii::app()->name;
?>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'     => 'login-form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array(
        'class' => 'form-signin'
    ),
)); ?>
    <h2 class="form-signin-heading">Please sign in</h2>

    <?= $form->errorSummary($model) ?>
    <?= $form->textField($model, 'username', array('class' => 'input-block-level', 'placeholder' => 'Email')); ?>
    <?= $form->passwordField($model, 'password', array('class' => 'input-block-level', 'placeholder' => 'Password')); ?>
    <label class="checkbox">
        <input type="checkbox" value="remember-me"> Remember me
    </label>
    <?= TbHtml::submitButton('Sign in', array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'class' => 'btn-large')); ?>

<?php $this->endWidget(); ?>