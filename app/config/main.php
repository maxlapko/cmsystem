<?php
/**
 *
 * main.php configuration file
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
defined('APP_CONFIG_NAME') or define('APP_CONFIG_NAME', 'main');

use Yiinitializr\Helpers\ArrayX;

// web application configuration
return ArrayX::merge(array(
    'name' => '{APPLICATION NAME}',
    
    // path aliases
    'aliases' => array(),
    
    'behaviors' => array(
        'runEnd' => array('class' => 'ext.behaviors.WebApplicationEndBehavior'),
    ),
    // controllers mappings
    'controllerMap' => array(),

    // application components
    'components' => array(
        'bootstrap' => array(
            'class' => 'vendor.clevertech.yii-booster.src.components.Bootstrap',
        ),
        'user' => array(
            'allowAutoLogin' => true,
            'class' => 'ext.components.WebUser'
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),        
        'image' => array(
            'class'        => 'ext.components.image_processor.MImageProcessor',
            'imagePath'    => 'webroot.files.images', //save images to this path    
            'imageUrl'     => '/files/images/',
            'fileMode'     => 0777,
            'imageHandler' => array(
                'class'  => 'ext.components.image_processor.image_handler.MImageHandler',
                'driver' => 'MDriverImageMagic', // MDriverGD
            ),
            'forceProcess'       => true,
            'afterUploadProcess' => array(
                'condition' => array('maxWidth' => 1024, 'maxHeight' => 768),
                'actions'   => array(
                    'resize' => array('width' => 1024, 'height' => 768),
                )
            ),
            'presets' => array(
                'image_preview' => array(
                    'thumb' => array('width'  => 100, 'height' => 100)
                ),
                'image_media_preview' => array(
                    'thumb' => array('width'  => 175, 'height' => 175)
                ),
            ),
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(                
                '/' => 'site/index',
                '/pages/<slug:[-\w]+>' => 'pages/pages/show',                
                '/blog/category/<code:[-\w]+>' => 'blog/posts/index',
                '/blog' => 'blog/posts/index',
                '/blog/<slug:[-\w]+>' => 'blog/posts/show',
                '/glossary' => 'glossary/items/index',
                
                '<_m:\w+>/<_c:\w+>/<id:\d+>' => '<_m><_c>/view',
                '<_m:\w+>/<_c:\w+>/<_a:\w+>/<id:\d+>' => '<_m>/<_c>/<_a>',
                '<_m:\w+>/<_c:\w+>/<_a:\w+>' => '<_m>/<_c>/<_a>',
                
                '<_c:\w+>/<id:\d+>' => '<_c>/view',
                '<_c:\w+>/<_a:\w+>/<id:\d+>' => '<_c>/<_a>',
                '<_c:\w+>/<_a:\w+>' => '<_c>/<_a>',                
                
            ),
        ),
    ),
    // application parameters
    'params' => array(),
), require(dirname(__FILE__) . '/common.php'));