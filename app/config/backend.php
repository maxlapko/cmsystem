<?php

return array(
    'name' => 'CMSystem',
    'preload' => array('bootstrap'),
    'import' => array(
        'application.modules.admins.models.*',
    ),
    'components' => array(
        'urlManager' => array(
            'showScriptName' => true,
            'rules' => array(
                '/' => 'admins/admins/admin',
            )
        ),
        'user' => array(
            'stateKeyPrefix' => 'backend',
            'isAdmin'        => true,
        )
    ),
);