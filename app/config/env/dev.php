<?php

return array(
	'modules' => array(
		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'password' => 'yii',
			'ipFilters' => array('127.0.0.1','::1'),
		),
	),
	'components' => array(
        'log' => array(
//            'routes' => array(
//                array(
//                    'class' => 'vendor.malyshev.yii-debug-toolbar.yii-debug-toolbar.YiiDebugToolbarRoute',
//                ),
//            )
        ),
//		modify to suit your needs
//		'db' => array(
//			'connectionString' => 'mysql:host=localhost;dbname=cms',
//			'username' => '{USERNAME}',
//			'password' => '{PASSWORD}',
//			'enableProfiling' => true,
//			'enableParamLogging' => true,
//			'charset' => 'utf8',
//		),
	),
	'params' => array(
		'yii.handleErrors'   => true,
		'yii.debug' => true,
		'yii.traceLevel' => 3,
        'backend.link' => '/backend.php'
	)
);