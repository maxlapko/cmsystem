<?php

return array(
    'theme' => 'default',
    'components' => array(
        'urlManager' => array(
            'class' => 'ext.components.I18nUrlManager',
            'showScriptName' => false,
        ),
        'admin' => array(
            'class' => 'ext.components.WebUser',
            'stateKeyPrefix' => 'backend',
        )
    ),
);