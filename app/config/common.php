<?php

require_once dirname(__FILE__) . '/../helpers/common.php';

return array(
    'basePath' => realPath(__DIR__ . '/..'),
    'sourceLanguage' => 'en',
    'preload' => array('log'),
    'aliases' => array(
        'vendor' => 'application.lib.vendor'
    ),
    'import' => array(
        'application.controllers.*',
        'application.helpers.*',
        'application.models.*',
        'application.models.forms.*',
        'application.modules.pages.models.*',
        'ext.components.*',
        'ext.behaviors.*',
        'ext.widget.*',
        'ext.components.yii-mail.*',
    ),
    // application modules
    'modules' => array(
        'admins',
        'mail',
        'menu',
        'pages',
        'blog',
        'languages',
        'images',
        'gallery',
        'categories',
        'contentblock',
        'settings',
        'glossary',
        'translate',
        'search' => array(
            'searchModules' => array('pages', 'blog')
        )
    ),
    'components' => array(
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                array(
                    'class'  => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
        'mail' => array(
            'class' => 'ext.components.yii-mail.YiiMail',
            'transportType' => 'php',
            //'transportOptions' => '-fnoreply@test.com',
            'viewPath' => 'application.views.mails',
            'logging' => false,
            'dryRun' => false
        ),
        'cache' => array(
            'class' => 'CDummyCache'
        ),
    ),
    'params' => array_merge(
        array(
            // php configuration
            'php.defaultCharset' => 'utf-8',
            'php.timezone'       => 'UTC',
            'registredWidgets'   => array(),
            'backend.link'       => '/backend.php'
        ),
        require dirname(__FILE__) . '/params.php'
    )
);