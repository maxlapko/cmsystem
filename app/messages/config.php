<?php

return array(
    'sourcePath' => dirname(__FILE__) . '/..',
    'messagePath' => dirname(__FILE__) . '/../messages', 
    'languages' => array('ru'), // according to your translation needs
    'fileTypes' => array('php'),
    'overwrite' => false,
    'removeOld' => true,
    'sort' => true,
    'exclude' => array(
        '.svn',
        '.gitignore',
        'yiilite.php',
        'yiit.php',
        '/messages',
        '/lib',
        '/runtime',
        '/config',
        '/extensions/behaviors',
        '/extensions/components',
    ),
);
