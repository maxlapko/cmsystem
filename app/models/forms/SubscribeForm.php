<?php

/**
 * Login form 
 */
class SubscribeForm extends CFormModel
{

    public $email;

        /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // username and password are required
            array('email', 'required'),
            array('email', 'email'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'email' => 'Email',
        );
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
    public function perform()
    {
        if ($this->validate()) {
            Yii::import('ext.components.mailchimp.MCAPI');
            $mc = new MCAPI(param('mailchimp.apikey'));
            $mc->listSubscribe(param('mailchimp.list'), $this->email);
            if ($mc->errorCode) {
                $this->addError('email', $mc->errorMessage);
                return false;
            }
            return true;
        }
        return false;
    }

}
